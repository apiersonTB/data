$fromaddress = "noreply@ticketboat.com" 
$toaddress = "<apierson@ticketboat.com>" 
$Subject = "Ticket Evolution Report" 
$body = "The Quickbooks file should be uploaded to QB, Make sure to check the error files. - Andrew Pierson"
$smtpserver = "smtp.office365.com"  

$message = new-object System.Net.Mail.MailMessage 
$message.From = $fromaddress 
$message.To.Add($toaddress)
$message.IsBodyHtml = $True 
$message.Subject = $Subject

#your file location
$files=Get-ChildItem "F:\TB-Applications\Data\Deployed\TicketEvolutionReport\Quickbooks and Errors"
Foreach($file in $files)
{
Write-Host �Attaching File :- � $file
$attachment = new-object System.Net.Mail.Attachment -ArgumentList $file.FullName
$message.Attachments.Add($attachment)
}

$message.body = $body 
$smtp = new-object Net.Mail.SmtpClient($smtpserver, 587)
$smtp.EnableSsl = $true 
$smtp.Credentials = New-Object System.Net.NetworkCredential(�noreply@ticketboat.com�, �RetroG@mes�);  
$smtp.Send($message)
