import hmac
import json
import pytz
import sys
import pyodbc
import base64
import os.path
import smtplib
import hashlib
import pandas as pd
from requests import get
from datetime import timedelta
from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart
from email.mime.application import MIMEApplication

#Define the function for pulling data from SQL Server database
def data_pull(server='192.168.10.203', database='eiboxoffice', username='appuser', password='RetroG@mes', query=''):
    cnxn = pyodbc.connect('DRIVER={ODBC Driver 13 for SQL Server};SERVER='+server+';DATABASE='+database+';UID='+username
                          + ';PWD=' + password)
    return pd.read_sql_query(query, cnxn)

#Define query to pull TEVO POs and event info
TEVOPOs = """SELECT E.[EventName]
	  ,V.[VenueName]
	  ,BOTG.[EventDate]
	  ,BO.[BuyOrderID]
      ,BO.[PONumber]
      ,BO.[TotalAmount]
      ,BO.[BalanceAmount]
      ,BO.[BalanceDisplayAmount]
      ,BO.[FinalizedDateTime]
      ,BO.[ShippedDateTime]
      ,CASE WHEN BO.[CancelDateTime] IS NOT NULL THEN '1' ELSE '0' END AS 'IsCanceled'
      ,BO.[ExternalReferenceNumber]
  FROM [eiboxoffice].[dbo].[BuyOrder] BO
  LEFT JOIN dbo.BuyOrderTicketGroup BOTG ON BOTG.BuyOrderID = BO.BuyOrderID
  LEFT JOIN dbo.Event E ON E.EventID = BOTG.PrimaryEventID
  LEFT JOIN dbo.Venue V ON V.VenueID = BOTG.VenueID
  WHERE BO.FinalizedDateTime > GETDATE() - 8
  AND BO.ExternalReferenceNumber LIKE '%-%'
  AND BO.ExternalReferenceNumber NOT LIKE '%/%'
  AND BO.ExternalReferenceNumber NOT LIKE 'TS%'
  AND BO.ExternalReferenceNumber NOT LIKE '% %'
  AND BO.ExternalReferenceNumber NOT LIKE '%E-Delivery%'
  AND BO.ExternalReferenceNumber NOT LIKE '-%'
  AND BO.BillingCompany LIKE '%Ticket E%'"""
TEVOPOS = data_pull(server='192.168.10.203', query=TEVOPOs)

#Filter the query to yesterday to time script is ran
import datetime
yesterday = datetime.date.today()- timedelta(1)
yesterday = yesterday.strftime('%m/%d/%Y')
TEVOPOS['FinalizedDate'] = pd.to_datetime(TEVOPOS['FinalizedDateTime']).apply(lambda x: x.strftime('%m/%d/%Y'))
TEVOPOS = TEVOPOS[TEVOPOS['FinalizedDate'] >= yesterday]
TEVOPOS = TEVOPOS.drop(['FinalizedDate'], axis = 1)


#Call the Ticket Evolution API
date = str(datetime.date.today()- timedelta(2))
Data = []
ID = []
Customer = []
Date = []
BuyerState = []
SellerState = []
Total = []
Balance = []
FirstEventAt = []
pages = [str(i) for i in range(1,10)]
for page in pages:
    url = 'api.ticketevolution.com/v9/orders?created_at.gte='+date+'&page='+page+'&per_page=20'
    secret = bytes('k9T8MQck91x2Gf6XfCvV2q6FNBzQsz8ffh0BdjPz', 'utf-8')
    signature_parameters = bytes('GET '+url, 'utf-8')
    strsig = base64.b64encode(hmac.new(secret, signature_parameters, digestmod=hashlib.sha256).digest())
    headers = {'X-Token': "a4b6023144057ad226bf5a48c3e9cb27",
               'Authorization': "Bearer k9T8MQck91x2Gf6XfCvV2q6FNBzQsz8ffh0BdjPz",
               'cache-control': "no-cache", 'Postman-Token': "3fa70c3f-cfaf-4772-bc42-0c2eed774d83", "X-Signature": strsig}
    response = get('http://'+url, headers=headers)
    response = response.text
    d2 = json.loads(response)
    Data.extend(d2['orders'])
    for i in d2['orders']:
        try:
            ID.append(i['oid'])
        except:
            ID.append(i['oid'])
        try:
            Customer.append(i['buyer']['brokerage']['name'])
        except:
            Customer.append(i['buyer']['brokerage']['name'])
        try:
            Date.append(i['created_at'])
        except:
            Date.append(i['created_at'])
        try:
            BuyerState.append(i['buyer_state'])
        except:
            BuyerState.append(i['buyer_state'])
        try:
            SellerState.append(i['seller_state'])
        except:
            SellerState.append(i['seller_state'])
        try:
            Total.append(i['total'])
        except:
            Total.append(i['total'])
        try:
            Balance.append(i['balance'])
        except:
            Balance.append(i['balance'])
        try:
            FirstEventAt.append(i['items'][0]['ticket_group']['event']['occurs_at'])
        except:
            FirstEventAt.append(i['items'][0]['ticket_group']['event']['occurs_at'])

TE = pd.DataFrame()
TE['ID'] = ID
TE['Customer'] = Customer
TE['Date'] = Date
TE['Buyer State'] = BuyerState
TE['Seller State'] = SellerState
TE['Total'] = Total
TE['Balance'] = Balance
TE['Event Date'] = FirstEventAt

#Convert Time
TE['Date'] = pd.to_datetime(TE['Date'])
from datetime import datetime
def is_dst(zonename):
    tz = pytz.timezone(zonename)
    now = pytz.utc.localize(datetime.utcnow())
    return now.astimezone(tz).dst() != timedelta(0)

dst = is_dst("US/Central")

def convert_time(times):
    if dst == 1:
        return times - timedelta(hours = 5)
    else:
        return times - timedelta(hours = 6)

TE['Date'] = convert_time(TE['Date'])
TE['DateONLY'] = pd.to_datetime(TE['Date']).apply(lambda x: x.strftime('%m/%d/%Y'))
TE['Date'] = pd.to_datetime(TE['Date']).apply(lambda x: x.strftime('%m/%d/%Y %H:%M:%S'))


#Filter TEVO POs to yesterday and Customer to Ticket Evolution
import datetime
yesterday = datetime.date.today()- timedelta(1)
yesterday = yesterday.strftime('%m/%d/%Y')
TE = TE[TE['DateONLY'] == yesterday]
TE = TE[TE['Customer'] == 'Ticket Boat']

#Create dataframe for the canceled and rejected to see credits
TE2 = TE[TE['Seller State'] == 'canceled']
TEVOPOS2 = TEVOPOS[TEVOPOS['IsCanceled'] == 1]
TEVO2 = TE2.merge(TEVOPOS2, how='outer', left_on='ID', right_on='ExternalReferenceNumber')
TEVO_CreditPOErrors = TEVO2[TEVO2['PONumber'].isnull()]
eiBO_CreditPOErrors = TEVO2[TEVO2['ID'].isnull()]
TEVO_CreditPOErrors = TEVO_CreditPOErrors[['ID', 'Customer', 'Date', 'Buyer State', 'Seller State', 'Total', 'Balance', 'Event Date']]
eiBO_CreditPOErrors = eiBO_CreditPOErrors[['EventName', 'VenueName', 'EventDate', 'BuyOrderID', 'PONumber', 'TotalAmount', 'BalanceAmount',
                   'FinalizedDateTime', 'ExternalReferenceNumber']]

#Filter rejected and canceled for matching QB entries
TE = TE[TE['Seller State'] != 'rejected']
TE = TE[TE['Seller State'] != 'canceled']

#Merge the POs from EIBO
TEVO = TE.merge(TEVOPOS, how='left', left_on='ID', right_on='ExternalReferenceNumber')

#Rearrange, drop unnecessary columns, and add replicated information for formatting
TEVO = TEVO.drop(columns = ['Customer', 'Buyer State', 'Seller State', 'Balance', 'Event Date', 'EventName',
                            'VenueName', 'EventDate', 'BuyOrderID', 'TotalAmount', 'BalanceAmount', 'FinalizedDateTime',
                            'ShippedDateTime', 'ExternalReferenceNumber'])
TEVO['Payee'] = 'TEVOLUTION'
TEVO['Account'] = "12000 " + u'\u00B7' + " Inventory-Tickets:12100 " + u'\u00B7' + " PO'd Inventory"
TEVO = TEVO[['DateONLY', 'PONumber', 'Payee', 'Account', 'Total', 'ID']]
TEVO.rename(columns={'DateONLY':'Date'}, inplace=True)
TEVO.dropna(subset=['PONumber'], inplace=True)


ERRORS = TE.merge(TEVOPOS, how='outer', left_on='ID', right_on='ExternalReferenceNumber')
ERRORS1 = ERRORS[ERRORS['PONumber'].isnull()]
EiboPO_ERRORS = ERRORS1[['EventName', 'VenueName', 'EventDate', 'BuyOrderID', 'PONumber', 'TotalAmount', 'BalanceAmount',
                   'FinalizedDateTime', 'ExternalReferenceNumber']]
ERRORS2 = ERRORS[ERRORS['ID'].isnull()]
TEvoPO_ERRORS = ERRORS2[['ID', 'Customer', 'Date', 'Buyer State', 'Seller State', 'Total', 'Balance', 'Event Date']]

#Check to see if the charge and credit amounts are the same
POCreditMatches = ERRORS[(ERRORS['PONumber'].notnull()) & (ERRORS['ID'].notnull())]
POCreditMatches['Total'] = pd.to_numeric(POCreditMatches['Total'])
POCreditMatches['TotalAmount'] = pd.to_numeric(POCreditMatches['TotalAmount'])
POCreditMatches['Total Sale Match'] = POCreditMatches['Total'].equals(POCreditMatches['TotalAmount'])
POCreditMatches = POCreditMatches[POCreditMatches['Total Sale Match'] == 0]
eiBOCreditAmountErrors = POCreditMatches[['EventName', 'VenueName', 'EventDate', 'BuyOrderID', 'PONumber', 'TotalAmount', 'BalanceAmount',
                   'FinalizedDateTime', 'ExternalReferenceNumber']]
TEVOCreditAmountErrors = POCreditMatches[['ID', 'Customer', 'Date', 'Buyer State', 'Seller State', 'Total', 'Balance', 'Event Date']]

#Check to see if the charge and credit amounts are the same
POChargeMatches = TEVO2[(TEVO2['PONumber'].notnull()) & (TEVO2['ID'].notnull())]
POChargeMatches['Total'] = pd.to_numeric(POChargeMatches['Total'])
POChargeMatches['TotalAmount'] = pd.to_numeric(POChargeMatches['TotalAmount'])
POChargeMatches['Total Sale Match'] = POChargeMatches['Total'].equals(POChargeMatches['TotalAmount'])
POChargeMatches = POChargeMatches[POChargeMatches['Total Sale Match'] == 0]
eiBOChargeAmountErrors = POChargeMatches[['EventName', 'VenueName', 'EventDate', 'BuyOrderID', 'PONumber', 'TotalAmount', 'BalanceAmount',
                   'FinalizedDateTime', 'ExternalReferenceNumber']]
TEVOChargeAmountErrors = POChargeMatches[['ID', 'Customer', 'Date', 'Buyer State', 'Seller State', 'Total', 'Balance', 'Event Date']]


#Create Excel Workbook for QuickBooks
writer1 = pd.ExcelWriter('F:/TB-Applications/Data/Deployed/TicketEvolutionReport/Quickbooks and Errors/TicketEvolutionPOs.xlsx', engine = 'xlsxwriter')
TEVO.to_excel(writer1, sheet_name = 'TEVO', index = False, encoding='utf-8-sig')
workbook1 = writer1.book
worksheet1 = writer1.sheets['TEVO']
worksheet1.set_column('A:H', 20)
writer1.save()


#Create Excel Workbook for Charge Errors if they exist
if len(EiboPO_ERRORS) > 1 or len(TEvoPO_ERRORS) > 0:
    writer2 = pd.ExcelWriter('F:/TB-Applications/Data/Deployed/TicketEvolutionReport/Quickbooks and Errors/ChargeErrors.xlsx', engine = 'xlsxwriter')
    EiboPO_ERRORS.to_excel(writer2, sheet_name = 'eiBO PO Charge Errors', index = False, encoding='utf-8-sig')
    TEvoPO_ERRORS.to_excel(writer2, sheet_name = 'TEVO PO Charge Errors', index = False, encoding='utf-8-sig')
    workbook2 = writer2.book
    worksheet2 = writer2.sheets['eiBO PO Charge Errors']
    worksheet3 = writer2.sheets['TEVO PO Charge Errors']
    worksheet2.set_column('A:T', 20)
    worksheet3.set_column('A:T', 20)
    writer2.save()
else:
    print('No Charge Errors')

#Create Excel Workbook for Credit Errors if they exist
if len(eiBO_CreditPOErrors) > 0 or len(TEVO_CreditPOErrors) > 0:
    writer3 = pd.ExcelWriter('F:/TB-Applications/Data/Deployed/TicketEvolutionReport/Quickbooks and Errors/CreditErrors.xlsx', engine = 'xlsxwriter')
    eiBO_CreditPOErrors.to_excel(writer3, sheet_name = 'eiBO PO Credit Errors', index = False, encoding='utf-8-sig')
    TEVO_CreditPOErrors.to_excel(writer3, sheet_name = 'TEVO PO Credit Errors', index = False, encoding='utf-8-sig')
    workbook3 = writer3.book
    worksheet4 = writer3.sheets['eiBO PO Credit Errors']
    worksheet5 = writer3.sheets['TEVO PO Credit Errors']
    worksheet4.set_column('A:T', 20)
    worksheet5.set_column('A:T', 20)
    writer3.save()
else:
    print('No Credit Errors')

#Create Excel Workbook for Charge Amount Errors if they exist
if len(eiBOCreditAmountErrors) > 0 or len(TEVOCreditAmountErrors) > 0:
    writer4 = pd.ExcelWriter('F:/TB-Applications/Data/Deployed/TicketEvolutionReport/Quickbooks and Errors/ChargeAmountErrors.xlsx', engine = 'xlsxwriter')
    eiBOCreditAmountErrors.to_excel(writer4, sheet_name = 'eiBO Charge Amount Errors', index = False, encoding='utf-8-sig')
    TEVOCreditAmountErrors.to_excel(writer4, sheet_name = 'TEVO Charge Amount Errors', index = False, encoding='utf-8-sig')
    workbook4 = writer4.book
    worksheet6 = writer4.sheets['eiBO Charge Amount Errors']
    worksheet7 = writer4.sheets['TEVO Charge Amount Errors']
    worksheet6.set_column('A:T', 20)
    worksheet7.set_column('A:T', 20)
    writer4.save()
else:
    print('No Charge Amount Errors')

#Create Excel Workbook for Credit Amount Errors if they exist
if len(eiBOChargeAmountErrors['PONumber']) > 0 or len(TEVOChargeAmountErrors['ID']) > 0:
    writer5 = pd.ExcelWriter('F:/TB-Applications/Data/Deployed/TicketEvolutionReport/Quickbooks and Errors/CreditAmountErrors.xlsx', engine = 'xlsxwriter')
    eiBOChargeAmountErrors.to_excel(writer5, sheet_name = 'eiBO Credit Amount Errors', index = False, encoding='utf-8-sig')
    TEVOChargeAmountErrors.to_excel(writer5, sheet_name = 'TEVO Credit Amount Errors', index = False, encoding='utf-8-sig')
    workbook5 = writer5.book
    worksheet8 = writer5.sheets['eiBO Credit Amount Errors']
    worksheet9 = writer5.sheets['TEVO Credit Amount Errors']
    worksheet8.set_column('A:T', 20)
    worksheet9.set_column('A:T', 20)
    writer5.save()
else:
    print('No Credit Amount Errors')

#Remove the report files from the folder
CreditAmountErrors = 'F:/TB-Applications/Data/Deployed/TicketEvolutionReport/Quickbooks and Errors/CreditAmountErrors.xlsx'
ChargeAmountErrors = 'F:/TB-Applications/Data/Deployed/TicketEvolutionReport/Quickbooks and Errors/ChargeAmountErrors.xlsx'
CreditErrors = 'F:/TB-Applications/Data/Deployed/TicketEvolutionReport/Quickbooks and Errors/CreditErrors.xlsx'
ChargeErrors = 'F:/TB-Applications/Data/Deployed/TicketEvolutionReport/Quickbooks and Errors/ChargeErrors.xlsx'

if os.path.exists(CreditAmountErrors):
    os.remove(CreditAmountErrors)
else:
    print('"Can not delete CreditAmountErrors as it doesnt exist"')

if os.path.exists(ChargeAmountErrors):
    os.remove(ChargeAmountErrors)
else:
    print('"Can not delete ChargeAmountErrors as it doesnt exist"')

if os.path.exists(CreditErrors):
    os.remove(CreditErrors)
else:
    print('"Can not delete CreditErrors as it doesnt exist"')

if os.path.exists(ChargeErrors):
    os.remove(ChargeErrors)
else:
    print('"Can not delete ChargeErrors as it doesnt exist"')

# Connect to the Outlook server
admin_email = 'noreply@ticketboat.com'
admin_password = 'RetroG@mes'
smtpObj = smtplib.SMTP('smtp-mail.outlook.com', 587)
smtpObj.ehlo()
smtpObj.starttls()
smtpObj.login(admin_email, admin_password)
dir_path = 'F:/TB-Applications/Data/Deployed/TicketEvolutionReport/Quickbooks and Errors'
files = os.listdir('F:/TB-Applications/Data/Deployed/TicketEvolutionReport/Quickbooks and Errors/')
for f in files:
    # Create message container - the correct MIME type is multipart/alternative.
    body = 'The Quickbooks file should be uploaded to QB, Make sure to check the error files.'
    msg = MIMEMultipart('alternative')
    msg['Subject'] = "Ticket Evolution Report"
    msg['From'] = admin_email
    master_recipients = ['apierson@ticketboat.com', 'lindsay@ticketboat.com', 'lgregerson@ticketboat.com']
    msg['To'] = ", ".join(master_recipients)
    file_path = os.path.join(dir_path, f)
    attachment = MIMEApplication(open(file_path, "rb").read(), _subtype='xlsx')
    attachment.add_header('Content-Disposition', 'attachment', filename=f)
    email_body = MIMEText(body, 'plain')
    msg.attach(attachment)
    msg.attach(email_body)
    smtpObj.sendmail(admin_email, master_recipients, msg.as_string())

#Finish and close terminal
print("Report Complete -- Close Window")
sys.exit()