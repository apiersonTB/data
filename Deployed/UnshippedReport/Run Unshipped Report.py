#Read in libraries
import csv
import re
import json
import time
import glob
import pytz
import pyodbc
import smtplib
import requests
import http.client
import numpy as np
import pandas as pd
import os, os.path, sys
from dateutil import tz
from requests import get
from datetime import datetime
from selenium import webdriver
from datetime import timedelta
import xml.etree.ElementTree as ET
from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart
from email.mime.application import MIMEApplication

userProfilePath = os.path.expanduser('~')

#Define Date Constraints for AP Data
start1 = datetime.now()
end1 = datetime.now() + timedelta(89)
start1 = start1.strftime('%m/%d/%Y')
end1 = end1.strftime('%m/%d/%Y')

#Export and download AP Unshipped data frame
browser = webdriver.Chrome()
browser.maximize_window()
browser.get('https://members.autoprocessor.com/Account/Login')

browser.implicitly_wait(10)

user = browser.find_element_by_id('Username')
password = browser.find_element_by_id('Password')
login_button = browser.find_element_by_xpath('/html/body/div[3]/form/div[5]/button')

user.send_keys('buying@ticketboat.com')
password.send_keys('Chiefs88')
login_button.click()

time.sleep(6)

order_manager_selector = browser.find_element_by_xpath('//*[@id="SideMenu1"]/li[4]/a/span[1]')
order_manager_selector.click()

search_button = browser.find_element_by_xpath(
    '//*[@id="orderManagerPage"]/div/div/div[1]/article/section[5]/button[1]')

time.sleep(1)

all_but_fulfilled_button = browser.find_element_by_xpath('//*[@id="orderManagerPage"]/div/div/div[1]/article/section[4]/div[3]/div/select/option[2]')
all_but_fulfilled_button.click()

time.sleep(1)

event_date_button = browser.find_element_by_xpath('//*[@id="orderManagerPage"]/div/div/div[1]/article/section[2]/div[1]/input')
event_date_button.click()

time.sleep(1)

start_date = browser.find_element_by_xpath('//*[@id="startDate"]')
start_date.click()
start_date.clear()
start_date.send_keys(start1)

end_date = browser.find_element_by_xpath('//*[@id="stopDate"]')
end_date.click()
end_date.clear()
end_date.send_keys(end1)
search_button.click()

time.sleep(2)

download_csv_button = browser.find_element_by_xpath(
    '//*[@id="orderManagerPage"]/div/div/div[2]/div/div/apweb-grid-standalone/div/div[1]/a[1]')
download_csv_button.click()

browser.implicitly_wait(10)
browser.close()

time.sleep(3)

#Retrieve the latest download file path
list_of_files = glob.glob(userProfilePath + '/Downloads/*.csv')
latest_file = max(list_of_files, key=os.path.getctime)
APDATA1 = pd.read_csv(latest_file, encoding='cp1252')
APDATA1 = APDATA1.loc[(APDATA1['Marketplace'] == 'Vivid Seats') | (APDATA1['Marketplace'] == 'StubHub') | (APDATA1['Marketplace'] == 'TicketNetwork Direct')]
APDATA1 = APDATA1.loc[APDATA1['Internal Status'] == 'All But Fulfilled']
APDATA1 = APDATA1.rename(index=str, columns={"Marketplace Order Id":"externalreferencenumber"})
os.remove(latest_file)

#Define Date Constraints for AP Data
start2 = datetime.now() + timedelta(91)
end2 = datetime.now() + timedelta(178)
start2 = start2.strftime('%m/%d/%Y')
end2 = end2.strftime('%m/%d/%Y')

#Export and download AP Unshipped data frame
browser = webdriver.Chrome()
browser.maximize_window()
browser.get('https://members.autoprocessor.com/Account/Login')

browser.implicitly_wait(10)

user = browser.find_element_by_id('Username')
password = browser.find_element_by_id('Password')
login_button = browser.find_element_by_xpath('/html/body/div[3]/form/div[5]/button')

user.send_keys('buying@ticketboat.com')
password.send_keys('Chiefs88')
login_button.click()

time.sleep(6)

order_manager_selector = browser.find_element_by_xpath('//*[@id="SideMenu1"]/li[4]/a/span[1]')
order_manager_selector.click()

search_button = browser.find_element_by_xpath(
    '//*[@id="orderManagerPage"]/div/div/div[1]/article/section[5]/button[1]')

time.sleep(1)

all_but_fulfilled_button = browser.find_element_by_xpath('//*[@id="orderManagerPage"]/div/div/div[1]/article/section[4]/div[3]/div/select/option[2]')
all_but_fulfilled_button.click()

event_date_button = browser.find_element_by_xpath('//*[@id="orderManagerPage"]/div/div/div[1]/article/section[2]/div[1]/input')
event_date_button.click()

time.sleep(1)

start_date = browser.find_element_by_xpath('//*[@id="startDate"]')
start_date.click()
start_date.clear()
start_date.send_keys(start2)

end_date = browser.find_element_by_xpath('//*[@id="stopDate"]')
end_date.click()
end_date.clear()
end_date.send_keys(end2)
search_button.click()

time.sleep(2)

download_csv_button = browser.find_element_by_xpath(
    '//*[@id="orderManagerPage"]/div/div/div[2]/div/div/apweb-grid-standalone/div/div[1]/a[1]')
download_csv_button.click()

browser.implicitly_wait(10)
browser.close()

time.sleep(3)

#Retrieve the latest download file path
list_of_files = glob.glob(userProfilePath + '/Downloads/*.csv')
latest_file = max(list_of_files, key=os.path.getctime)
APDATA2 = pd.read_csv(latest_file, encoding='cp1252')
APDATA2 = APDATA2.loc[(APDATA2['Marketplace'] == 'Vivid Seats') | (APDATA2['Marketplace'] == 'StubHub') | (APDATA2['Marketplace'] == 'TicketNetwork Direct')]
APDATA2 = APDATA2.loc[APDATA2['Internal Status'] == 'All But Fulfilled']
APDATA2 = APDATA2.rename(index=str, columns={"Marketplace Order Id":"externalreferencenumber"})
os.remove(latest_file)

#Define Date Constraints for AP Data
start3 = datetime.now() + timedelta(180)
end3 = datetime.now() + timedelta(269)
start3 = start3.strftime('%m/%d/%Y')
end3 = end3.strftime('%m/%d/%Y')

#Export and download AP Unshipped data frame
browser = webdriver.Chrome()
browser.maximize_window()
browser.get('https://members.autoprocessor.com/Account/Login')

browser.implicitly_wait(10)

user = browser.find_element_by_id('Username')
password = browser.find_element_by_id('Password')
login_button = browser.find_element_by_xpath('/html/body/div[3]/form/div[5]/button')

user.send_keys('buying@ticketboat.com')
password.send_keys('Chiefs88')
login_button.click()

time.sleep(6)

order_manager_selector = browser.find_element_by_xpath('//*[@id="SideMenu1"]/li[4]/a/span[1]')
order_manager_selector.click()

search_button = browser.find_element_by_xpath(
    '//*[@id="orderManagerPage"]/div/div/div[1]/article/section[5]/button[1]')

time.sleep(1)

all_but_fulfilled_button = browser.find_element_by_xpath('//*[@id="orderManagerPage"]/div/div/div[1]/article/section[4]/div[3]/div/select/option[2]')
all_but_fulfilled_button.click()

time.sleep(1)

event_date_button = browser.find_element_by_xpath('//*[@id="orderManagerPage"]/div/div/div[1]/article/section[2]/div[1]/input')
event_date_button.click()

time.sleep(1)

start_date = browser.find_element_by_xpath('//*[@id="startDate"]')
start_date.click()
start_date.clear()
start_date.send_keys(start3)

end_date = browser.find_element_by_xpath('//*[@id="stopDate"]')
end_date.click()
end_date.clear()
end_date.send_keys(end3)
search_button.click()

time.sleep(2)

download_csv_button = browser.find_element_by_xpath(
    '//*[@id="orderManagerPage"]/div/div/div[2]/div/div/apweb-grid-standalone/div/div[1]/a[1]')
download_csv_button.click()

browser.implicitly_wait(10)
browser.close()

time.sleep(3)

#Retrieve the latest download file path
list_of_files = glob.glob(userProfilePath + '/Downloads/*.csv')
latest_file = max(list_of_files, key=os.path.getctime)
APDATA3 = pd.read_csv(latest_file, encoding='cp1252')
APDATA3 = APDATA3.loc[(APDATA3['Marketplace'] == 'Vivid Seats') | (APDATA3['Marketplace'] == 'StubHub') | (APDATA3['Marketplace'] == 'TicketNetwork Direct')]
APDATA3 = APDATA3.loc[APDATA3['Internal Status'] == 'All But Fulfilled']
APDATA3 = APDATA3.rename(index=str, columns={"Marketplace Order Id":"externalreferencenumber"})
os.remove(latest_file)

#Define Date Constraints for AP Data
start4 = datetime.now() + timedelta(271)
end4 = datetime.now() + timedelta(360)
start4 = start4.strftime('%m/%d/%Y')
end4 = end4.strftime('%m/%d/%Y')

#Export and download AP Unshipped data frame
browser = webdriver.Chrome()
browser.maximize_window()
browser.get('https://members.autoprocessor.com/Account/Login')

browser.implicitly_wait(10)

user = browser.find_element_by_id('Username')
password = browser.find_element_by_id('Password')
login_button = browser.find_element_by_xpath('/html/body/div[3]/form/div[5]/button')

user.send_keys('buying@ticketboat.com')
password.send_keys('Chiefs88')
login_button.click()

time.sleep(6)

order_manager_selector = browser.find_element_by_xpath('//*[@id="SideMenu1"]/li[4]/a/span[1]')
order_manager_selector.click()

search_button = browser.find_element_by_xpath(
    '//*[@id="orderManagerPage"]/div/div/div[1]/article/section[5]/button[1]')

time.sleep(1)

all_but_fulfilled_button = browser.find_element_by_xpath('//*[@id="orderManagerPage"]/div/div/div[1]/article/section[4]/div[3]/div/select/option[2]')
all_but_fulfilled_button.click()

time.sleep(1)

event_date_button = browser.find_element_by_xpath('//*[@id="orderManagerPage"]/div/div/div[1]/article/section[2]/div[1]/input')
event_date_button.click()

time.sleep(1)

start_date = browser.find_element_by_xpath('//*[@id="startDate"]')
start_date.click()
start_date.clear()
start_date.send_keys(start4)


end_date = browser.find_element_by_xpath('//*[@id="stopDate"]')
end_date.click()
end_date.clear()
end_date.send_keys(end4)
search_button.click()

time.sleep(2)

download_csv_button = browser.find_element_by_xpath(
    '//*[@id="orderManagerPage"]/div/div/div[2]/div/div/apweb-grid-standalone/div/div[1]/a[1]')
download_csv_button.click()

time.sleep(3)
browser.implicitly_wait(10)
browser.close()
browser.quit()

time.sleep(3)

#Retrieve the latest download file path
list_of_files = glob.glob(userProfilePath + '/Downloads/*.csv')
latest_file = max(list_of_files, key=os.path.getctime)
APDATA4 = pd.read_csv(latest_file, encoding='cp1252')
APDATA4 = APDATA4.loc[(APDATA4['Marketplace'] == 'Vivid Seats') | (APDATA4['Marketplace'] == 'StubHub') | (APDATA4['Marketplace'] == 'TicketNetwork Direct')]
APDATA4 = APDATA4.loc[APDATA4['Internal Status'] == 'All But Fulfilled']
APDATA4 = APDATA4.rename(index=str, columns={"Marketplace Order Id":"externalreferencenumber"})
os.remove(latest_file)

APDATA = APDATA1.append(APDATA2)
APDATA = APDATA.append(APDATA3)
APDATA = APDATA.append(APDATA4)
APDATA.to_csv('F:/TB-Applications/Data/Deployed/UnshippedReport/unshippedreportoutputfiles/APDATA.csv', index=False)

print("downloaded files")
#Define the function for pulling data from SQL Server database
def data_pull(server='192.168.10.203', database='eiboxoffice', username='appuser', password='RetroG@mes', query=''):
    cnxn = pyodbc.connect('DRIVER={ODBC Driver 13 for SQL Server};SERVER='+server+';DATABASE='+database+';UID='+username
                          + ';PWD=' + password)
    return pd.read_sql_query(query, cnxn)

#Define query to pull NIH and empty shipping info
TB_Unshipped_query = """SELECT sv.[externalreferencenumber]
      ,sv.[eventname]
      ,sv.[venuename]
      ,sv.[eventdate]
      ,sv.[eventtime]
      ,sv.inhanddate
      ,sv.IsCanceled
  FROM [eiboxoffice].[dbo].[tb_salesview] sv (NOLOCK)
  JOIN dbo.sellorder s (NOLOCK) ON sv.sellorderid = s.sellorderid
  JOIN dbo.shippingmethod sm (NOLOCK) ON s.shippingmethodid = sm.shippingmethodid
  LEFT OUTER JOIN Sold.STG.reporting.reportnotes rn (NOLOCK) ON cast(sv.ticketgroupid AS varchar(20)) = rn.uniquevalue
  WHERE sv.ShippingTrackingCode LIKE '%NIH%'
  AND (billingattn LIKE '%stub%' OR billingattn LIKE '%Ticketmaster%' OR billingattn LIKE '%Vivid%' OR billingattn LIKE '%TND%')
  AND sv.EventDate >= GETDATE()
"""

TBNIH = data_pull(server='192.168.10.203', query=TB_Unshipped_query)
TBNIH.to_csv('F:/TB-Applications/Data/Deployed/UnshippedReport/unshippedreportoutputfiles/TBNIH.csv', index = False)


#API call for StubHub
def data_pull(server='192.168.10.209', database='STG', username='appuser', password='RetroG@mes', query=''):
    cnxn = pyodbc.connect('DRIVER={ODBC Driver 13 for SQL Server};SERVER='+server+';DATABASE='+database+';UID='+username+';PWD='+ password)
    return pd.read_sql_query(query, cnxn)

token_query = 'SELECT * FROM [STG].[api].[tokens]'
token = data_pull(server='192.168.10.209', query=token_query).loc[2].value

print("About to do call SH")
conn = http.client.HTTPSConnection("api.stubhub.com")

headers = {
    'Authorization': "Bearer " + token,
    'Cache-Control': "no-cache",
    'Postman-Token': "5d0fb3d0-43e9-4c17-825f-6679bd0a3756"
    }

print("Call SH")

def api_call(max_length_per_call):
    req = conn.request("GET",
            "/accountmanagement/sales/v1/seller/6C21FF9F4E1D3BC0E04400144FB7AAA6?filters=status:CONFIRMED&start="
                       +str(max_length_per_call)+"&end="+str(max_length_per_call + 250), headers=headers)
    res = conn.getresponse()
    data = res.read()
    data = data.decode("utf-8")
    data = pd.read_json(data, orient = 'index')
    return data

sales = api_call(0)['sale']
print("Called SH")

for i in range(1,12):
    sales += api_call((i*250)+1)['sale']

sales.to_json('F:/TB-Applications/Data/Deployed/UnshippedReport/unshippedreportoutputfiles/SHUF.json', orient='index')
df = pd.read_json('F:/TB-Applications/Data/Deployed/UnshippedReport/unshippedreportoutputfiles/SHUF.json')


toCSV = df['sales']
keys = toCSV[0].keys()
with open('F:/TB-Applications/Data/Deployed/UnshippedReport/unshippedreportoutputfiles/SHUF.csv', 'w', newline = "") as output_file:
    dict_writer = csv.DictWriter(output_file, keys, extrasaction = 'ignore')
    dict_writer.writeheader()
    dict_writer.writerows(toCSV)


#API call for VividSeats
conn = http.client.HTTPSConnection("brokers.vividseats.com")

headers = {
    'Cache-Control': "no-cache",
    'Postman-Token': "a6877d71-dc0f-4971-a10f-4b63a55c227a"
    }

V = conn.request("GET",
                 "/webservices/v1/getOrders?apiToken=2493e445-e5c2-41f0-9d84-a7a6c9b8c238&status=PENDING_SHIPMENT",
                 headers=headers)

res = conn.getresponse()
VSUF = res.read()
VSUF = VSUF.decode("utf-8")
print(VSUF)


class XML2DataFrame:

    def __init__(self, xml_data):
        self.root = ET.XML(xml_data)

    def parse_root(self, root):
        return [self.parse_element(child) for child in iter(root)]

    def parse_element(self, element, parsed=None):
        if parsed is None:
            parsed = dict()
        for key in element.keys():
            parsed[key] = element.attrib.get(key)
        if element.text:
            parsed[element.tag] = element.text
        for child in list(element):
            self.parse_element(child, parsed)
        return parsed

    def process_data(self):
        structure_data = self.parse_root(self.root)
        return pd.DataFrame(structure_data)


xml2df = XML2DataFrame(VSUF)
xml_dataframe = xml2df.process_data()
xml_dataframe.to_csv('F:/TB-Applications/Data/Deployed/UnshippedReport/unshippedreportoutputfiles/VSUS.csv', index=False)


print("About to call TM")
#API call for Ticketmaster
url = "https://api.eventinventory.com/BrokerAPI/V1/Orders/"
querystring = {"securityToken":"21775A047A9F4227B71711E6E7FB658B","brokerId":"1395","orderStatuses":"10,101"}
payload = ""
headers = {
    'cache-control': "no-cache",
    'Postman-Token': "1f745628-015e-448f-995f-8edf544d54a3"
    }
response = requests.request("GET", url, data=payload, headers=headers, params=querystring)
response = response.content.decode("utf-8")
print("Called TM")

column_headers = []
root = ET.fromstring(response)

for i in root[1][0]:
    column_headers.append(re.sub(r"\s*{.*}\s*", " ", i.tag))

with open('F:/TB-Applications/Data/Deployed/UnshippedReport/unshippedreportoutputfiles/TMRTF.csv', 'w', newline='') as file:
    writer = csv.writer(file)
    writer.writerow(column_headers)
    for i in root[1]:
        temp = []
        for x in i:
            temp.append(x.text)
        writer.writerow(temp)

file.close()

print("About to call TND")
#API call for Ticket Network Direct Listings
headers = {'X-Identity-Context': "broker-id=1368", 'Authorization': "Bearer 6d11d008-e91a-3c68-a813-17046c69fb61",
    'cache-control': "no-cache", 'Postman-Token': "d631c6c3-e20f-4828-9081-cfb13e9e3ace"}
data = []
ticketGroupNotes = []
requestedDeliveryMethod = []
eventDate = []
venueName = []
eventName = []
invoiceDate = []
customerName = []
estimatedArrivalDate = []
trackingNumber = []
statusDescription = []
section = []
row = []
pages = [str(i) for i in range(0,15)]
for page in pages:
    response = get("https://www.tn-apis.com/order/v4/orders?$filter=delivery/status/id eq 1&perPage=500&page=" + page, headers = headers )
    response = response.text
    d2 = json.loads(response)
    data.extend(d2['results'])
    for i in d2['results']:
        try:
            requestedDeliveryMethod.append(i['ticketRequest']['requestedDeliveryMethod']['description'])
        except:
            requestedDeliveryMethod.append('NA')
        try:
            ticketGroupNotes.append(i['ticketRequest']['ticketGroupNotes'])
        except:
            ticketGroupNotes.append('NA')
        try:
            eventDate.append(i['soldTicketGroups'][0]['event']['date'])
        except:
            eventDate.append('NA')
        try:
            venueName.append(i['soldTicketGroups'][0]['event']['venue']['name'])
        except:
            venueName.append('NA')
        try:
            eventName.append(i['soldTicketGroups'][0]['event']['name'])
        except:
            eventName.append('NA')
        try:
            invoiceDate.append(i['invoice']['date'])
        except:
            invoiceDate.append('NA')
        try:
            customerName.append(i['customer']['name'])
        except:
            customerName.append('NA')
        try:
            estimatedArrivalDate.append(i['delivery']['estimatedArrivalDate'])
        except:
            estimatedArrivalDate.append('NA')
        try:
            trackingNumber.append(i['delivery']['label']['trackingNumber'])
        except:
            trackingNumber.append('NA')
        try:
            statusDescription.append(i['status']['description'])
        except:
            statusDescription.append('NA')
        try:
            section.append(i['soldTicketGroups'][0]['seats']['section'])
        except:
            section.append('NA')
        try:
            row.append(i['soldTicketGroups'][0]['seats']['row'])
        except:
            row.append('NA')
TNDPD = pd.DataFrame(data)
TNDPD['requestedDeliveryMethodDescription'] = requestedDeliveryMethod
TNDPD['ticketGroupNotes'] = ticketGroupNotes
TNDPD['eventdate'] = eventDate
TNDPD['venue'] = venueName
TNDPD['event'] = eventName
TNDPD['invoiceDate'] = invoiceDate
TNDPD['customerName'] = customerName
TNDPD['estimatedArrivalDate'] = estimatedArrivalDate
TNDPD['trackingNumber'] = trackingNumber
TNDPD['statusDescription'] = statusDescription
TNDPD['section'] = section
TNDPD['row'] = row
TNDPD = TNDPD[TNDPD.eventdate != 'NA']
TNDPD.to_csv('F:/TB-Applications/Data/Deployed/UnshippedReport/unshippedreportoutputfiles/TNDPD.csv')


#Read in the NIH SQL query
TBNIH = pd.read_csv('F:/TB-Applications/Data/Deployed/UnshippedReport/unshippedreportoutputfiles/TBNIH.csv')


from pytz import timezone
#Read in and filter StubHub data
from_zone = tz.gettz('UTC')
to_zone = tz.gettz('America/Chicago')
tz = timezone('America/Chicago')
SHUF = pd.read_csv('F:/TB-Applications/Data/Deployed/UnshippedReport/unshippedreportoutputfiles/SHUF.csv', encoding='cp1252')
SHUF['eventTime'] = [x.split('T')[1] for x in SHUF['eventDate']]
SHUF['eventTime'] = pd.to_datetime(SHUF['eventDate'])

def is_dst(zonename):
    tz = pytz.timezone(zonename)
    now = pytz.utc.localize(datetime.utcnow())
    return now.astimezone(tz).dst() != timedelta(0)

dst = is_dst("US/Central")

def convert_time(times):
    if dst == 1:
        return times - timedelta(hours = 5)
    else:
        return times - timedelta(hours = 6)

SHUF['eventTime'] = convert_time(SHUF['eventTime'])
SHUF['eventTime'] = [d.time() for d in SHUF['eventTime']]

SHUF['eventDate'] = [x.split('T')[0] for x in SHUF['eventDate']]
SHUF['eventDate'] = pd.to_datetime(SHUF['eventDate'])
yesterday = pd.datetime.today() - timedelta(1)
SHUF = SHUF.loc[SHUF['eventDate'] >= yesterday]
SHUF = SHUF.rename(index=str, columns={"saleId":"externalreferencenumber"})
SHUF = SHUF.merge(TBNIH, how='left', on = 'externalreferencenumber')
SHUF = SHUF[SHUF.IsCanceled != 1]
SHUF['eventDate'] = [d.date() for d in SHUF['eventDate']]

#Read in and filter VividSeats data
VSUS = pd.read_csv('F:/TB-Applications/Data/Deployed/UnshippedReport/unshippedreportoutputfiles/VSUS.csv')
VSUS['eventDate'] = pd.to_datetime(VSUS['eventDate'])
VSUS = VSUS.loc[VSUS['eventDate'] >= yesterday]
VSUS = VSUS.rename(index=str, columns={"orderId":"externalreferencenumber"})
VSUS = VSUS.merge(TBNIH, how='left', on = 'externalreferencenumber')
VSUS = VSUS[VSUS.IsCanceled != 1]


#Read in and filter Ticketmaster data
TMRTF = pd.read_csv('F:/TB-Applications/Data/Deployed/UnshippedReport/unshippedreportoutputfiles/TMRTF.csv', encoding='cp1252')
TMRTF[' EventDate'] = [x.split('T')[0] for x in TMRTF[' EventDate']]
TMRTF[' EventDate'] = pd.to_datetime(TMRTF[' EventDate'])
TMRTF = TMRTF.loc[TMRTF[' EventDate'] >= yesterday]
TMRTF = TMRTF.rename(index=str, columns={" PurchaseOrderNumber":"externalreferencenumber"})
TMRTF = TMRTF.merge(TBNIH, how='left', on = 'externalreferencenumber')
TMRTF = TMRTF[TMRTF.IsCanceled != 1]


#Read in and filter Ticket Network Direct data
TNDPD['eventdate'] = pd.to_datetime(TNDPD['eventdate'])
TNDPD = TNDPD.loc[TNDPD['eventdate'] >= yesterday]
TNDPD['referenceId'] = TNDPD['referenceId'].astype(int)
TNDPD = TNDPD.rename(index=str, columns={"referenceId":"externalreferencenumber"})
TNDPD = TNDPD.merge(TBNIH, how='left', on = 'externalreferencenumber')
TNDPD = TNDPD[TNDPD.IsCanceled != 1]
TNDPD = TNDPD.rename(index=str, columns={"eventdate_x":"eventdate"})

#Read in and filter EIBO data
EIBO = pd.read_csv('F:/TB-Applications/Data/Deployed/UnshippedReport/unshippedreportoutputfiles/TBNIH.csv', encoding='cp1252')
EIBO = EIBO[EIBO.IsCanceled != 1]
#Combine the list of external reference ids from the exchanges and vlookup cells on external reference number
XCHNG = np.concatenate([SHUF['externalreferencenumber'], VSUS['externalreferencenumber'],
                                                   TNDPD['externalreferencenumber'], TMRTF['externalreferencenumber']])
EIBO['Exchange Shipped Status'] = EIBO.externalreferencenumber.isin(XCHNG).astype(int)


#Vlookup cells on External Rerference ID and return a boolean value
VSUS['EIBO Shipped Status'] = VSUS.externalreferencenumber.isin(TBNIH.externalreferencenumber).astype(int)
TMRTF['EIBO Shipped Status'] = TMRTF.externalreferencenumber.isin(TBNIH.externalreferencenumber).astype(int)
TNDPD['EIBO Shipped Status'] = TNDPD.externalreferencenumber.isin(TBNIH.externalreferencenumber).astype(int)
SHUF['EIBO Shipped Status'] = SHUF.externalreferencenumber.isin(TBNIH.externalreferencenumber).astype(int)


#Vlookup cells on External Rerference ID and return a boolean value
VSUS['AP Shipped Status'] = VSUS.externalreferencenumber.isin(APDATA.externalreferencenumber).astype(int)
TMRTF['AP Shipped Status'] = TMRTF.externalreferencenumber.isin(APDATA.externalreferencenumber).astype(int)
TNDPD['AP Shipped Status'] = TNDPD.externalreferencenumber.isin(APDATA.externalreferencenumber).astype(int)
SHUF['AP Shipped Status'] = SHUF.externalreferencenumber.isin(APDATA.externalreferencenumber).astype(int)


#Reorganize column order, rename headers, replace boolean values with shipped status for Ticket Network
TNDPD = TNDPD[['AP Shipped Status', 'EIBO Shipped Status', 'externalreferencenumber', 'invoiceDate', 'event',
               'eventdate', 'venue', 'customerName', 'estimatedArrivalDate', 'trackingNumber', 'statusDescription',
               'ticketQuantity', 'section', 'row', 'onHandDate']]
TNDPD.rename(columns={'externalreferencenumber':'Order Id', 'invoiceDate':'Order Date', 'event':'Event',
               'eventdate':'Event Date', 'venue':'Venue', 'customerName':'Customer',
               'estimatedArrivalDate':'Delivery Date', 'trackingNumber':'Tracking Number', 'statusDescription':'Status',
               'ticketQuantity':'Quantity', 'section':'Section', 'row':'Row', 'onHandDate':'On Hand Date'},
               inplace=True)
TNDPD['EIBO Shipped Status'] = TNDPD['EIBO Shipped Status'].replace([1, 0],['', 'Shipped'])
TNDPD['AP Shipped Status'] = TNDPD['AP Shipped Status'].replace([1, 0],['', 'Shipped'])
TNDPD = TNDPD.drop_duplicates(['Order Id'])

#Reorganize column order, rename headers, replace boolean values with shipped status for StubHub and format datetime
SHUF = SHUF[['AP Shipped Status', 'EIBO Shipped Status', 'saleDate', 'eventDate', 'eventTime', 'eventDescription',
             'venueDescription', 'externalreferencenumber', 'inhandDate', 'section', 'row', 'seats',
             'fulfillmentMethodDisplayName', 'quantity']]
SHUF.rename(columns={'saleDate':'Sale Date', 'eventDate':'Event Date', 'eventTime':'Event Time',
                    'eventDescription':'Event', 'venueDescription':'Venue', 'externalreferencenumber':'Transaction ID',
                    'inhandDate':'In-Hand Date', 'section':'Section', 'row':'Row', 'seats':'Seats',
                    'fulfillmentMethodDisplayName':'Fulfillment Method', 'quantity':'Quantity'}, inplace=True)
SHUF['EIBO Shipped Status'] = SHUF['EIBO Shipped Status'].replace([1, 0],['', 'Shipped'])
SHUF['AP Shipped Status'] = SHUF['AP Shipped Status'].replace([1, 0],['', 'Shipped'])
SHUF = SHUF.drop_duplicates(['Transaction ID'])

#Reorganize column order, rename headers, replace boolean values with shipped status on Ticketmaster and format datetime
TMRTF = TMRTF[['AP Shipped Status', 'EIBO Shipped Status', 'externalreferencenumber', ' OrderDate', ' EventName',
               ' VenueName', ' EventDate', ' ShipDate', ' TotalCost', ' Quantity', ' ShippingMethodName', ' Section',
               ' Row', ' StartSeat', ' EndSeat', ' InHandDate']]
TMRTF.rename(columns={'externalreferencenumber':'Order ID', ' OrderDate':'Order Date', ' EventName':'Event',
               ' VenueName':'Venue', ' EventDate':'Event Date', ' ShipDate':'Ship Date', ' TotalCost': 'Cost',
               ' ShippingMethodName':'Shipping Method'}, inplace=True)
TMRTF['EIBO Shipped Status'] = TMRTF['EIBO Shipped Status'].replace([1, 0], ['', 'Shipped'])
TMRTF['AP Shipped Status'] = TMRTF['AP Shipped Status'].replace([1, 0], ['', 'Shipped'])
TMRTF = TMRTF.drop_duplicates(['Order ID'])

#Reorganize column order, rename headers, replace boolean values with shipped status for VividSeats and format datetime
VSUS = VSUS[['AP Shipped Status', 'EIBO Shipped Status', 'externalreferencenumber', 'orderDate', 'event',
             'eventDate', 'quantity', 'section', 'row', 'expectedShipDate']]
VSUS.rename(columns={'externalreferencenumber':'ID', 'orderDate':'Order Date', 'event':'Event', 'eventDate':'Event Date',
                    'quantity':'Quantity', 'section':'Section', 'row':'Row', 'expectedShipDate':'Date Expected'},
                    inplace=True)
VSUS['EIBO Shipped Status'] = VSUS['EIBO Shipped Status'].replace([1, 0], ['', 'Shipped'])
VSUS['AP Shipped Status'] = VSUS['AP Shipped Status'].replace([1, 0], ['', 'Shipped'])
VSUS = VSUS.drop_duplicates(['ID'])

EIBO = EIBO[['Exchange Shipped Status', 'externalreferencenumber', 'eventname', 'venuename', 'eventdate', 'eventtime',
             'inhanddate']]
EIBO['Exchange Shipped Status'] = SHUF['EIBO Shipped Status'].replace([1, 0],['', 'Shipped'])

#Export dataframes to sheets in an excel workbook and modify format
writer = pd.ExcelWriter('F:/TB-Applications/Data/Deployed/UnshippedReport/unshippedreportoutputfiles/Unshipped Report/UnshippedReport.xlsx', engine = 'xlsxwriter')
SHUF.to_excel(writer, sheet_name = 'StubHub', index = False)
TNDPD.to_excel(writer, sheet_name = 'Ticket Network Direct', index = False)
TMRTF.to_excel(writer, sheet_name = 'Ticketmaster', index = False)
VSUS.to_excel(writer, sheet_name = 'VividSeats', index = False)
EIBO.to_excel(writer, sheet_name = 'eiBox Office', index = False)
workbook = writer.book
worksheet1 = writer.sheets['StubHub']
worksheet2 = writer.sheets['Ticket Network Direct']
worksheet3 = writer.sheets['Ticketmaster']
worksheet4 = writer.sheets['VividSeats']
worksheet5 = writer.sheets['eiBox Office']
worksheet1.set_column('A:H', 20)
worksheet2.set_column('A:H', 20)
worksheet3.set_column('A:H', 20)
worksheet4.set_column('A:H', 20)
worksheet5.set_column('A:H', 20)
writer.save()
print("Report Complete -- Close Window")

# Connect to the Outlook server
admin_email = 'noreply@ticketboat.com'
admin_password = 'RetroG@mes'
smtpObj = smtplib.SMTP('smtp-mail.outlook.com', 587)
smtpObj.ehlo()
smtpObj.starttls()
smtpObj.login(admin_email, admin_password)

# Create message container - the correct MIME type is multipart/alternative.
body = 'Please find the Unshipped Report attached to this email.'
msg = MIMEMultipart('alternative')
msg['Subject'] = "Unshipped Report"
msg['From'] = admin_email
master_recipients = ['apierson@ticketboat.com', 'emmag@ticketboat.com', 'lindsay@ticketboat.com',
                     'ckesner@ticketboat.com', 'lgregerson@ticketboat.com']
msg['To'] = ", ".join(master_recipients)
attachment = MIMEApplication(open('F:/TB-Applications/Data/Deployed/UnshippedReport/unshippedreportoutputfiles/Unshipped Report/UnshippedReport.xlsx', 'rb').read(), _subtype='csv')
attachment.add_header('Content-Disposition', 'attachment', filename='UnshippedReport.xlsx')
attachment2 = MIMEApplication(open('F:/TB-Applications/Data/Deployed/UnshippedReport/unshippedreportoutputfiles/Unshipped Report/Unshipped Report Memo.docx', 'rb').read(), _subtype='doc')
attachment2.add_header('Content-Disposition', 'attachment', filename='Unshipped Report Memo.docx')
email_body = MIMEText(body, 'plain')
msg.attach(attachment)
msg.attach(attachment2)
msg.attach(email_body)
smtpObj.sendmail(admin_email, master_recipients, msg.as_string())

sys.exit()