#Read in libraries
from selenium import webdriver
import pandas as pd
import datetime
import pytz
from datetime import datetime, timedelta
import glob
import os.path
import time
import pyodbc

userProfilePath = os.path.expanduser('~')

#Define Date Constraints for AP Data
start1 = datetime.now() - timedelta(31)
end1 = datetime.now()
start1 = start1.strftime('%m/%d/%Y')
end1 = end1.strftime('%m/%d/%Y')

#Export and download AP Unshipped data frame
browser = webdriver.Chrome()
browser.maximize_window()
browser.get('https://members.autoprocessor.com/Account/Login')

browser.implicitly_wait(10)

user = browser.find_element_by_id('Username')
password = browser.find_element_by_id('Password')
login_button = browser.find_element_by_xpath('/html/body/div[3]/form/div[5]/button')

user.send_keys('buying@ticketboat.com')
password.send_keys('Chiefs88')
login_button.click()

time.sleep(6)

order_manager_selector = browser.find_element_by_xpath('//*[@id="SideMenu1"]/li[4]/a/span[1]')
order_manager_selector.click()

time.sleep(1)

start_date = browser.find_element_by_xpath('//*[@id="startDate"]')
start_date.click()
start_date.clear()
start_date.send_keys(start1)

end_date = browser.find_element_by_xpath('//*[@id="stopDate"]')
end_date.click()
end_date.clear()
end_date.send_keys(end1)

time.sleep(5)

search_button = browser.find_element_by_xpath(
    '//*[@id="orderManagerPage"]/div/div/div[1]/article/section[5]/button[1]')
search_button.click()

time.sleep(6)

export_csv_button = browser.find_element_by_xpath(
    '//*[@id="orderManagerPage"]/div/div/div[2]/div/div/apweb-grid-standalone/div/div[1]/a[1]')
export_csv_button.click()

time.sleep(75)
browser.implicitly_wait(10)
browser.close()
browser.quit()

#Retrieve the latest download file path
list_of_files = glob.glob(userProfilePath + '/Downloads/*.csv')
latest_file = max(list_of_files, key=os.path.getctime)
APDATA = pd.read_csv(latest_file, encoding='cp1252')

#Change the datetime on the records to reflect Central Standard Time and drop duplicate invoices
APDATA['Created Date'] = pd.to_datetime(APDATA['Created Date'])

def is_dst(zonename):
    tz = pytz.timezone(zonename)
    now = pytz.utc.localize(datetime.utcnow())
    return now.astimezone(tz).dst() != timedelta(0)

dst = is_dst("US/Central")

def convert_time(times):
    if dst == 1:
        return times - timedelta(hours = 5)
    else:
        return times - timedelta(hours = 6)

APDATA['Created Date'] = convert_time(APDATA['Created Date'])
APDATA = APDATA.drop_duplicates(subset='Invoice #', keep='first')

#Subset the orders from the EIBO marketplace
EIBO = APDATA[APDATA['Marketplace'] == 'EI Box Office']

#Bring in the table
def data_pull(server='192.168.10.203', database='eiboxoffice', username='appuser', password='RetroG@mes', query=''):
    cnxn = pyodbc.connect('DRIVER={ODBC Driver 13 for SQL Server};SERVER='+server+';DATABASE='+database+';UID='+username
                          + ';PWD=' + password)
    return pd.read_sql_query(query, cnxn)
ordertypes = """SELECT InvoiceNumber 
,InternalNote
FROM eiboxoffice.dbo.TB_SalesView
WHERE FinalizedDateTime > GETDATE()-365"""
ordertypes = data_pull(server='192.168.10.203', query=ordertypes)

#Change the order types
ordertypes.loc[ordertypes.InternalNote.str.contains("Ships|SHIPS|@ST:1 @SM:2|@ST:4 @SM:11|@ST:4 @SM:12|@ST:1 @SM:3|HS", na=False), 'Fullfilment Type'] = 'Fedex'
ordertypes.loc[ordertypes.InternalNote.str.contains("TF|CBE|tF|Tf|PDF", na=False), 'Fullfilment Type'] = 'Pdf'
ordertypes.loc[ordertypes.InternalNote.str.contains("@ST:7 @SM:13|XFER", na=False), 'Fullfilment Type'] = 'Mobile'
ordertypes.loc[ordertypes.InternalNote.str.contains("@ST:5 @SM:15|FLS|Flash|ST:5 @SM:15", na=False), 'Fullfilment Type'] = 'Mobile'
ordertypes.loc[ordertypes.InternalNote.str.contains("@ST:7 @SM:14|@ST:7 @SM:15", na=False), 'Fullfilment Type'] = 'Barcode'

#Rename columns
ordertypes = ordertypes.rename(index=str, columns={"InvoiceNumber": "Invoice #"})
EIBO['Invoice #'] = EIBO['Invoice #'].astype(int)

#Join the order types to the list, fill empty values with default AP values
EIBO_Join = EIBO.merge(ordertypes, how='left', on = 'Invoice #')
EIBO_Join.loc[EIBO_Join["Fullfilment Type_y"].isnull(),'Fullfilment Type_y'] = EIBO_Join["Fullfilment Type_x"]
EIBO_Join = EIBO_Join.drop(["Fullfilment Type_x"], axis=1)
EIBO_Join = EIBO_Join.drop_duplicates(subset='Invoice #', keep='last')

#Rename and reorder columns
EIBO_Join = EIBO_Join.rename(index=str, columns={"Fullfilment Type_y": "Fullfilment Type"})
EIBO_Join = EIBO_Join[["Marketplace", "Order Status", "Internal Status", "Fullfilment Type", "Marketplace Order Id",
                      "Invoice #", "Created Date", "Event", "Venue", "Event Date", "Quantity", "Section", "Row",
                      "Seats", "Price Per", "Total", "In Hand Date"]]

#Subset the orders to exclude EIBO marketplace
APDATA = APDATA[APDATA['Marketplace'] != 'EI Box Office']

#Join the datasets together to get the final
APDATA = APDATA.append(EIBO_Join)
APDATA = APDATA.drop_duplicates(subset='Invoice #', keep='last')
APDATA.to_csv('C:/Users/administrator.TICKETBOAT0/Ticket Boat/Data Analytics - Documents/APDATA.csv', index=False)
os.remove(latest_file)