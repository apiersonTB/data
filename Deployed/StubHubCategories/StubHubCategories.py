#Read in libraries
from requests import get
import pandas as pd
import pyodbc
import json
from sqlalchemy import create_engine

#API call for StubHub
def data_pull(server='192.168.10.209', database='STG', username='appuser', password='RetroG@mes', query=''):
    cnxn = pyodbc.connect('DRIVER={ODBC Driver 13 for SQL Server};SERVER='+server+';DATABASE='+database+';UID='+username+';PWD='+ password)
    return pd.read_sql_query(query, cnxn)
token_query = 'SELECT * FROM [STG].[api].[tokens]'
token = data_pull(server='192.168.10.209', query=token_query).loc[2].value

#Call the StubHub categories API
Data = []
id = []
parentId = []
locale = []
name = []
description = []
parentName = []
url = 'https://api.stubhub.com/search/catalog/categories/v3?rows=500'
headers = {'Authorization': "Bearer " + token, 'Cache-Control': "no-cache",
           'Postman-Token': "5d0fb3d0-43e9-4c17-825f-6679bd0a3756"}
response = get(url, headers=headers)
response = response.text
d2 = json.loads(response)
Data.extend(d2['categories'])
for i in d2['categories']:
    try:
        id.append(i['id'])
    except:
        id.append('NA')
    try:
        parentId.append(i['parentId'])
    except:
        parentId.append('NA')
    try:
        locale.append(i['locale'])
    except:
        locale.append('NA')
    try:
        name.append(i['name'])
    except:
        name.append('NA')
    try:
        description.append(i['description'])
    except:
        description.append('NA')
    try:
        parentName.append(i['ancestors'][1]['name'])
    except:
        parentName.append('NA')
SHC = pd.DataFrame()
SHC['Category ID'] = id
SHC['Parent Category ID'] = parentId
SHC['Locale'] = locale
SHC['Category Name'] = name
SHC['Description'] = description
SHC['Parent Category Name'] = parentName

#Write to sql table
engine = create_engine("mssql+pyodbc://appuser:RetroG@mes@192.168.10.209/STG?DRIVER={ODBC Driver 13 for SQL Server}")
SHC.to_sql(name='stubhubcategories', con=engine, schema='api', if_exists='replace', index=False)