import pyodbc
import pandas as pd
import os.path
import win32com.client
import smtplib
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText

"""
    This file will go pull various queries from 209 and store the results in csv files. It will then run macros 
    contained in excel files to create reports that are formatted correctly and can be filtered. Then a final macro
    will go through and filter each file for each user and stack each report into a html file per user. Finally the
    html files are put into emails and sent to the users. - Derrickson
"""


def data_pull(server='192.168.10.209', database='STG', username='appuser', password='RetroG@mes', query=''):
    cnxn = pyodbc.connect('DRIVER={ODBC Driver 13 for SQL Server};SERVER='+server+';DATABASE='+database+';UID='+username+';PWD='+ password)
    return pd.read_sql_query(query, cnxn)

users_query = """
SELECT [lastname]
      ,[active]
FROM [STG].[users].[users]
WHERE active = 1"""

emails = {'Fortenberry': 'sfortenberry@ticketboat.com',
'Handler': 'shane@ticketboat.com',
'Harper': 'tharper@ticketboat.com',
'Hatfield': 'mike@ticketboat.com',
'Honeyman': 'choneyman@ticketboat.com',
'Hutson': 'zhutson@ticketboat.com',
'Joseph': 'seth@ticketboat.com',
'Kenney': 'akenney@ticketboat.com',
'Kern': 'jkern@ticketboat.com',
'Roque': 'sroque@ticketboat.com',
'Sharemet': 'rsharemet@ticketboat.com',
'Swinford': 'dswinford@ticketboat.com',
'Williams': 'blake@ticketboat.com'
}

admin_email = 'noreply@ticketboat.com'
admin_password = 'RetroG@mes'

# Next 30 Days Report
next_30_query = """
SELECT [userid]
      ,[lastname]
      ,[primaryeventid]
      ,[eventname]
      ,[venuename]
      ,[eventdate]
      ,[InvoiceNumber]
      ,[eventtime]
      ,[categoryname]
      ,[productionid]
      ,[FinalizedDateTime]
      ,[TotalTicketPrice]
      ,[isCanceled]
      ,[Quantity]
      ,[TotalTicketCost]
      ,[PONumber]
      ,[POFinalizedDateTime]
      ,[ClientId]
      ,[SeatSection]
      ,[StartingSeat]
      ,[EndingSeat]
      ,[SeatRow]
  FROM [kc].[eiboxoffice].[dbo].[TB_PricerViewAndrew]
  where POFinalizedDateTime >= DATEADD(day, -365, GETDATE()) and POFinalizedDateTime <= DATEADD(day, -335, GETDATE())
"""
next_30 = data_pull(query=next_30_query)
next_30.to_csv('F:/TB-Applications/Data/Deployed/LeadList/Next30Data.csv', index=False)
path_to_next30 = 'F:/TB-Applications/Data/Deployed/LeadList/Next30.xlsm'
if os.path.exists(path_to_next30):
    xl=win32com.client.Dispatch("Excel.Application")
    wb = xl.Workbooks.Open(os.path.abspath(path_to_next30))
    xl.Application.Run("Next30.xlsm!DataAutomation.next30")
    wb.Close(SaveChanges=1)
    del xl
print('Next 30 Report Complete')

# JIT Report
jit_query = """
WITH
   SaleCTE AS
   (
   
SELECT 
sv.primaryeventid
, sv.venueid
, sv.Eventdate
,sv.seatsection
,sv.seatrow
, ISNULL(sv.Eventtime, '12:00 AM') AS EventTime
, SUM(sv.Quantity)	Quantity
, SUM(sv.totalticketprice) Sales
, SUM(sv.totalticketcost) cost
, dbo.TB_GetMargin(SUM(sv.totalticketcost), SUM(sv.totalticketprice), 0) AS Margin
 FROM 

 clone.ticketgroup_recent sv (NOLOCK)
 
Where
DATEADD(dd, DATEDIFF(dd, 0, sv.Finalizeddatetime), 0) = dateadd(day,DATEDIFF(dd, 0, getdate()),-1)

GROUP BY sv.eventdate, sv.eventtime, sv.primaryeventid, sv.venueid, sv.seatsection,sv.seatrow
HAVING
dbo.TB_GetMargin(SUM(totalticketcost), SUM(totalticketprice), 0) > 26
AND sv.eventdate > DATEADD(WEEK,3, GETDATE())
 
 )
 ,
   InvCTE  AS
   (
	SELECT sv.PrimaryeventID, sv.venueid, sv.Eventdate,  ISNULL(sv.Eventtime, '12:00 AM') AS EventTime, SUM(sv.Quantity) InventoryQty
	FROM  clone.ticketgroup_recent	sv (NOLOCK)
	LEFT OUTER JOIN
	SaleCTE	sc (NOLOCK)
	ON sv.PrimaryeventID = sc.primaryeventid AND sv.eventdate = sc.eventdate
	WHERE
	sv.isComplete = '0'
	GROUP BY  sv.Eventdate,  ISNULL(sv.Eventtime, '12:00 AM'), sv.PrimaryeventID, sv.venueid
	HAVING
	sv.eventdate > DATEADD(WEEK,3, GETDATE())

   )
  ,

  
 Velocity AS
 (


 SELECT 
sv.primaryeventid
, sv.venueid
, sv.EventDate
,sv.seatsection
,sv.seatrow
, ISNULL(sv.Eventtime, '12:00 AM') AS EventTime
,SUM(sv.Quantity) TotalQtySold
, CAST(SUM(sv.Quantity)/NULLIF(CAST(DATEDIFF(d, CONVERT(VARCHAR(10),MIN(sv.createddatetime),110), GETDATE())AS FLOAT),0)AS FLOAT) AvgSalesPerDay
, DATEDIFF(d, CONVERT(VARCHAR(10),MIN(sv.createddatetime),110), GETDATE()) daysOnMkt
, MIN(sv.createddatetime) AS PoDate
 FROM 

 clone.ticketgroup_recent sv (NOLOCK)
 RIGHT OUTER JOIN SaleCTE	sc (NOLOCK)
	ON sv.PrimaryeventID = sc.primaryeventid AND sv.venueid = sc.venueid
 
WHERE
isComplete = '1'

GROUP BY sv.eventdate, ISNULL(sv.Eventtime, '12:00 AM'), sv.primaryeventid, sv.venueid,sv.seatsection,sv.seatrow
HAVING
sv.eventdate > DATEADD(WEEK,3, GETDATE())

)
,NoteCTE AS
(
	SELECT uniquevalue, note, ROW_NUMBER() OVER(PARTITION BY uniquevalue ORDER BY notedatetime DESC) AS isactive
	FROM  reporting.reportnotes WITH(NOLOCK)
	WHERE reportid = 39 
)


SELECT 
DISTINCT
u.firstname
,u.lastname
,tgr.eventname
, tgr.venuename
, s.EventDate AS EvtDate
,s.seatsection
,s.seatrow
,CONVERT(VARCHAR(10),s.Eventtime,108) AS EvtTime
, s.Quantity AS Qty
, s.Sales
, s.cost
, s.Margin
, I.InventoryQty AS InvQty
, v.TotalQtySold AS TtlQtySold
, CAST(ROUND(v.AvgSalesPerDay,4) AS NUMERIC(36,4)) AS AvgSalesPerDay
, v.daysOnmkt
, v.PoDate
,CONVERT(VARCHAR(10),rd.decisiondatetime,101) +' '+CONVERT(VARCHAR(10),rd.decisiondatetime,108) AS [TimeStamp]
,CASE
	WHEN rd.number1 = 1 THEN 'Buy'
	WHEN rd.number1 = 2 THEN 'Don''t Buy'
	WHEN rd.number1 = 3 THEN 'Bought'
	WHEN rd.number1 = 4 THEN 'Sold Out'
	WHEN rd.number1 = 5 THEN 'Add to Dropcatcher'
	ELSE 'None'
END AS buydecision

, rn.note AS LatestNote

 FROM 

 SaleCTE s (NOLOCK)
 LEFT OUTER JOIN  InvCTE I (NOLOCK)
	ON I.PrimaryEventID = s.PrimaryEventID AND I.eventdate = s.eventdate AND i.venueid=s.venueid AND s.eventtime = i.eventtime
 LEFT OUTER JOIN Velocity	v (NOLOCK)
	ON v.PrimaryEventID = s.PrimaryEventID AND v.eventdate = s.eventdate AND v.venueid=s.venueid AND s.eventtime = v.eventtime
 LEFT OUTER JOIN clone.ticketgroup_recent tgr (NOLOCK)
	ON tgr.PrimaryEventID = s.PrimaryEventID AND tgr.eventdate = s.eventdate AND tgr.venueid=s.venueid AND s.eventtime = ISNULL(tgr.Eventtime, '12:00 AM')
 LEFT OUTER JOIN NoteCTE rn (NOLOCK)
	ON rn.isactive = 1 AND CAST(s.primaryeventid AS VARCHAR(30)) + '-' + CAST(s.venueid AS VARCHAR(30)) + '-' + CAST((s.eventdate + s.eventtime) AS VARCHAR(30)) = rn.uniquevalue
 LEFT OUTER JOIN reporting.reportdata rd (NOLOCK)
	ON rd.reportid = 39 AND CAST(s.primaryeventid AS VARCHAR(30)) + '-' + CAST(s.venueid AS VARCHAR(30)) + '-' + CAST((s.eventdate + s.eventtime) AS VARCHAR(30)) = rd.uniquevalue 
--LEFT JOIN kc.eiboxoffice.dbo.production p (NOLOCK)
--	ON p.PrimaryEventID = s.PrimaryEventID 
--	--AND p.eventdate = s.eventdate AND p.venueid=s.venueid AND s.eventtime = p.eventtime
--LEFT JOIN users.productionpricerevent ppe (NOLOCK)
--	ON ppe.ProductionId = p.ProductionID
--LEFT JOIN users.users u (NOLOCK)
--	ON u.eiuserid = ppe.TempUserId
left join kc.eiboxoffice.dbo.Event e (NOLOCK) on e.eventid = s.primaryeventid
left join users.pricerevents pe on (NOLOCK) e.eventid = pe.eventid
left join users.users u (NOLOCK) on u.eiuserid = pe.userid

where u.active = 1

GROUP BY
u.firstname
,u.lastname
,tgr.eventname
, tgr.venuename
, s.EventDate
,s.Eventtime
, s.Quantity
, s.Sales
, s.cost
, s.Margin
, I.InventoryQty
, v.TotalQtySold
, v.AvgSalesPerDay
, v.daysOnmkt
, v.PoDate
,rd.decisiondatetime
,rd.number1
, rn.note
,s.seatsection
,s.seatrow
"""
jit = data_pull(query=jit_query)
jit.to_csv('F:/TB-Applications/Data/Deployed/LeadList/JustInTimeData.csv', index=False)
path_to_jit = 'F:/TB-Applications/Data/Deployed/LeadList/JustInTime.xlsm'
if os.path.exists(path_to_jit):
    xl=win32com.client.Dispatch("Excel.Application")
    wb = xl.Workbooks.Open(os.path.abspath(path_to_jit))
    xl.Application.Run("JustInTime.xlsm!DataAutomation.jit")
    wb.Close(SaveChanges=1)
    del xl
print('JIT Report Complete')

# Viagogo
viagogo_query = "SELECT * FROM [STG].[dbo].[skyboxsoldreport]"
viagogo = data_pull(query=viagogo_query)
viagogo.to_csv('F:/TB-Applications/Data/Deployed/LeadList/Viagogo.csv', index=False)
path_to_viagogo = 'F:/TB-Applications/Data/Deployed/LeadList/Viagogo.xlsm'
if os.path.exists(path_to_viagogo):
    xl = win32com.client.Dispatch("Excel.Application")
    wb = xl.Workbooks.Open(os.path.abspath(path_to_viagogo))
    xl.Application.Run("Viagogo.xlsm!DataAutomation.viagogo")
    wb.Close(SaveChanges=1)
    del xl
print('Viagogo Report Complete')

# Bought and Sold in Last 7 Days Report
last_7_query = """
SELECT [userid]
      ,[lastname]
      ,[primaryeventid]
      ,[eventname]
      ,[venuename]
      ,[eventdate]
      ,[InvoiceNumber]
      ,[eventtime]
      ,[categoryname]
      ,[productionid]
      ,[FinalizedDateTime]
      ,[TotalTicketPrice]
      ,[isCanceled]
      ,[Quantity]
      ,[TotalTicketCost]
      ,[PONumber]
      ,[POFinalizedDateTime]
      ,[ClientId]
      ,[SeatSection]
      ,[StartingSeat]
      ,[EndingSeat]
      ,[SeatRow]
  FROM [kc].[eiboxoffice].[dbo].[TB_PricerViewAndrew]
  where POFinalizedDateTime >= DATEADD(day, -7, GETDATE()) AND FinalizedDateTime >= DATEADD(day, -7, GETDATE())
"""
last_7 = data_pull(query=last_7_query)
last_7.to_csv('F:/TB-Applications/Data/Deployed/LeadList/Last7DaysData.csv', index=False)
path_to_last_7 = 'F:/TB-Applications/Data/Deployed/LeadList/Last7Days.xlsm'
if os.path.exists(path_to_last_7):
    xl=win32com.client.Dispatch("Excel.Application")
    wb = xl.Workbooks.Open(os.path.abspath(path_to_last_7))
    xl.Application.Run("Last7Days.xlsm!DataAutomation.last7")
    wb.Close(SaveChanges=1)
    del xl
print('Bought and Sold in Last 7 Days Report Complete')

# Tours To Go Through
onsale_event_assignment_query = """declare @rundate datetime = getDate() ;
                exec reporting.GetListDailyOnsaleSummary @date=@rundate"""
onsales_list = data_pull(server='192.168.10.209', query=onsale_event_assignment_query)
# SQL Query for the OnSales List for last seven days including today (Change getDate() to get another day)
for i in range(6):
    onsale_event_assignment_query = "declare @rundate datetime = getDate() - " + str(i + 1) + '; exec reporting.GetListDailyOnsaleSummary @date=@rundate'
    # Querying the database and writing the query to a dataframe for manipulation
    onsales_list_addon = data_pull(server='192.168.10.209', query=onsale_event_assignment_query)
    onsales_list.append(onsales_list_addon)
onsales_list.to_csv('F:/TB-Applications/Data/Deployed/LeadList/ToursToGoThroughData.csv', index=False)
path_to_tours_to_go_through = 'F:/TB-Applications/Data/Deployed/LeadList/ToursToGoThrough.xlsm'
if os.path.exists(path_to_tours_to_go_through):
    xl=win32com.client.Dispatch("Excel.Application")
    wb = xl.Workbooks.Open(os.path.abspath(path_to_tours_to_go_through))
    xl.Application.Run("ToursToGoThrough.xlsm!DataAutomation.tours")
    wb.Close(SaveChanges=1)
    del xl
print('Tours To Go Through Report Complete')

# Top Selling Events
top_selling_events_query = 'exec reporting.TopSellingEventsForDailyLeadList'

top_selling_events = data_pull(query=top_selling_events_query)
top_selling_events.to_csv('F:/TB-Applications/Data/Deployed/LeadList/TopSellingEventsData.csv', index=False)
path_to_top_selling_events = 'F:/TB-Applications/Data/Deployed/LeadList/TopSellingEvents.xlsm'
if os.path.exists(path_to_top_selling_events):
    xl=win32com.client.Dispatch("Excel.Application")
    wb = xl.Workbooks.Open(os.path.abspath(path_to_top_selling_events))
    xl.Application.Run("TopSellingEvents.xlsm!DataAutomation.topevents")
    wb.Close(SaveChanges=1)
    del xl
print('Top Selling Events Report Complete')

#Sheet to cyclethrough and create all of the HMTL documents for users
path_to_HTMLStacker = 'F:/TB-Applications/Data/Deployed/LeadList/HTMLStacker.xlsm'
if os.path.exists(path_to_HTMLStacker):
    xl=win32com.client.Dispatch("Excel.Application")
    wb = xl.Workbooks.Open(os.path.abspath(path_to_HTMLStacker))
    xl.Application.Run("HTMLStacker.xlsm!DataAutomation.cyclethrough")
    wb.Close(SaveChanges=1)
    del xl
print('Cycle-through of html files completed')

# Connect to the Outlook server
smtpObj = smtplib.SMTP('smtp-mail.outlook.com', 587)
smtpObj.ehlo()
smtpObj.starttls()
smtpObj.login(admin_email, admin_password)

for i in emails.keys():
    # Create the body of the message (a plain-text and an HTML version).
    html_file = open('F:/TB-Applications/Data/Deployed/LeadList/htmldocs/' + i + '.html', 'r')
    # Create message container - the correct MIME type is multipart/alternative.
    msg = MIMEMultipart('alternative')
    msg['Subject'] = "Daily Lead List"
    msg['From'] = admin_email
    msg['To'] = emails[i]
    # Record the MIME types of both parts - text/plain and text/html.
    email_body = MIMEText(html_file.read(), 'html')

    # Attach parts into message container.
    # According to RFC 2046, the last part of a multipart message, in this case the HTML message, is best and preferred.
    msg.attach(email_body)

    smtpObj.sendmail(admin_email, emails[i], msg.as_string())

# Create the master email for all events
master_html_file = open('F:/TB-Applications/Data/Deployed/LeadList/htmldocs/master.html', 'r')
master_msg = MIMEMultipart('alternative')
master_msg['Subject'] = "Overall Daily Lead List"
master_msg['From'] = admin_email
master_recipients = 'mark@ticketboat.com; sfortenberry@ticketboat.com; ckesner@ticketboat.com; mike@ticketboat.com; tderrickson@ticketboat.com'
master_msg['To'] = master_recipients
master_email_body = MIMEText(master_html_file.read(), 'html')
master_msg.attach(master_email_body)

smtpObj.sendmail(admin_email, master_recipients, master_msg.as_string())

smtpObj.quit()

print('finished')


