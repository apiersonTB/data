#Load libraries
from urllib import parse
import pandas as pd
import sqlalchemy
import urllib
import pyodbc

#Bring in the users table
def data_pull(server='192.168.10.209', database='STG', username='appuser', password='RetroG@mes', query=''):
    cnxn = pyodbc.connect('DRIVER={ODBC Driver 13 for SQL Server};SERVER='+server+';DATABASE='+database+';UID='+username
                          + ';PWD=' + password)
    return pd.read_sql_query(query, cnxn)
users = """SELECT [userid]
                 ,[login]
                 ,[firstname] + ' ' + [lastname] AS 'Employee'
                 ,[firstname]
                 ,[lastname]
                 ,[usergroupid]
                 ,[token]
                 ,[CompanyID]
                 ,[eiuserid]
                 ,[active]
           FROM [STG].[users].[users]
           WHERE [active] = 1"""
users = data_pull(server='192.168.10.209', query=users)

users_inactive = """SELECT [userid]
                 ,[login]
                 ,[firstname] + ' ' + [lastname] AS 'Employee'
                 ,[firstname]
                 ,[lastname]
                 ,[usergroupid]
                 ,[token]
                 ,[CompanyID]
                 ,[eiuserid]
                 ,[active]
           FROM [STG].[users].[users]
           WHERE [active] = 0"""
users_inactive = data_pull(server='192.168.10.209', query=users_inactive)

#Read in phone List
employees = pd.read_excel("C:/Users/administrator.TICKETBOAT0/Ticket Boat/Ticket Boat - Documents/TB Phone List & Schedule/PHONE LIST CURRENT.xlsx")

#Define the rows containing irrelevant information
corporate = employees.loc[employees['General Office Information'] == 'Corporate'].index.values.astype(int)
admin = employees[employees['General Office Information'] == 'Administration Department'].index.values.astype(int)
asset = employees[employees['General Office Information'] == 'Asset Department'].index.values.astype(int)
inventory = employees[employees['General Office Information'] == 'Inventory Department'].index.values.astype(int)
fulfillment = employees[employees['General Office Information'] == 'Fulfillment Department'].index.values.astype(int)
it = employees[employees['General Office Information'] == 'Information Technology Department'].index.values.astype(int)

#Drop rows
employees = employees.drop(index = it)
employees = employees.drop(index = corporate)
employees = employees.drop(index = admin)
employees = employees.drop(index = asset)
employees = employees.drop(index = inventory)
employees = employees.drop(index = fulfillment)

#Rename columns
employees = employees.rename(index=str, columns={"General Office Information": "Employee", "Office Lines": "Phone Numbers",
                                     "Office Main Email": "Email", "Address": "Address", "Fax #": "Position",
                                     "Department": "Department"})

#Join columns by names
employees = employees.merge(users, how='right', on = 'Employee')
employees = employees[['userid', 'Employee', 'firstname', 'lastname', 'login', 'eiuserid', 'active', 'Email',
         'Department', 'Position', 'CompanyID', 'Phone Numbers', 'usergroupid', 'token']]
users_inactive["Email"] = ""
users_inactive["Department"] = ""
users_inactive["Position"] = ""
users_inactive["Phone Numbers"] = ""
users_inactive = users_inactive[['userid', 'Employee', 'firstname', 'lastname', 'login', 'eiuserid', 'active', 'Email',
         'Department', 'Position', 'CompanyID', 'Phone Numbers', 'usergroupid', 'token']]
employees = employees.append(users_inactive)

#Create constraints for table creation to match size of records in order to avoid errors
tsql_chunksize = 2097 // len(employees.columns)
# cap at 1000 (limit for number of rows inserted by table-value constructor)
tsql_chunksize = 1000 if tsql_chunksize > 1000 else tsql_chunksize

#Name server attributes
server='192.168.10.209'
database='STG'
username='appuser'
password='RetroG@mes'
params = urllib.parse.quote_plus('DRIVER={ODBC Driver 13 for SQL Server};SERVER='+server+';DATABASE='+database+';UID='+username
                          + ';PWD=' + password)

#Publish table or refresh to update table
engine = sqlalchemy.create_engine("mssql+pyodbc:///?odbc_connect=%s" % params)
employees.to_sql("employeedirectory", engine, schema = 'users', index = False, if_exists = 'replace', chunksize=tsql_chunksize)