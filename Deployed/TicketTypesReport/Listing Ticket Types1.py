#Read in libraries
from requests import get
import pandas as pd
import requests
import pyodbc
import json
import time
import sys
import os

#Database Pull for Ticket Boat TicketGroups
def data_pull(server='192.168.10.203', database='eiboxoffice', username='appuser', password='RetroG@mes', query=''):
    cnxn = pyodbc.connect('DRIVER={ODBC Driver 13 for SQL Server};SERVER='+server+';DATABASE='+database+';UID='+username
                          + ';PWD=' + password)
    return pd.read_sql_query(query, cnxn)
TB_TicketType_query = """SELECT E.[EventName] AS eventName
	  ,TG.[TicketGroupID] AS externalListingId
      ,TG.[EventDate] AS eventDate
      ,TG.[Quantity] AS quantity
      ,TG.[InHandDate] AS inHandDate
      ,TG.[Description] AS description
      ,TG.[InternalNote] AS internalNote
  FROM [eiboxoffice].[dbo].[TicketGroup] TG
  LEFT JOIN [eiboxoffice].[dbo].[Event] E ON E.EventID = TG.[PrimaryEventID]
  LEFT JOIN [eiboxoffice].[dbo].[Venue] V ON V.VenueID = TG.[VenueID]
  WHERE TG.[EventDate] >= GETDATE()"""
TBType = data_pull(server='192.168.10.203', query=TB_TicketType_query)
TBType.loc[TBType.internalNote.str.contains("Ships|SHIPS|@ST:1 @SM:2|@ST:4 @SM:11|@ST:4 @SM:12|@ST:1 @SM:3|HS", na=False), 'TB Ticket Type'] = 'Mail'
TBType.loc[TBType.internalNote.str.contains("TF|CBE|tF|Tf|PDF", na=False), 'TB Ticket Type'] = 'TicketFast'
TBType.loc[TBType.internalNote.str.contains("@ST:7 @SM:13|XFER", na=False), 'TB Ticket Type'] = 'Mobile'
TBType.loc[TBType.internalNote.str.contains("@ST:5 @SM:15|FLS|Flash|ST:5 @SM:15", na=False), 'TB Ticket Type'] = 'Flash'
TBType.loc[TBType.internalNote.str.contains("@ST:7 @SM:14", na=False), 'TB Ticket Type'] = 'Barcode'
TBType.to_csv('F:/TB-Applications/Tableau/listingtickettypesoutputfiles/TBType.csv', index = False)

#API call for VividSeats Listings
url = "https://brokers.vividseats.com:443/webservices/listings/v2/get"
payload = ""
headers = {'Api-token': "2493e445-e5c2-41f0-9d84-a7a6c9b8c238",
           'cache-control': "no-cache", 'Postman-Token': "d49b9a77-465e-4a37-ba71-31d184f5c935"}
response = requests.request("GET", url, data=payload, headers=headers)
data = response.text
d2 = json.loads(data)
VS = pd.DataFrame(d2['listings'])
#VS.to_csv('C:/outputfiles/VS_Raw.csv', index=False)
VScolumns = ['attributes', 'city', 'cost', 'electronic', 'electronicRaw', 'electronicTransferRaw', 'faceValue',
             'hasFiles', 'instantDownload', 'internalNotes', 'instantDownload', 'lastUpdate', 'listDate',
             'passThrough', 'price', 'productionId', 'row', 'seatFrom', 'seatThru', 'section', 'splitType', 'splitValue',
             'state', 'tickets', 'spec']
VS.drop(VScolumns, axis=1, inplace=True)
VS = VS[['eventName', 'venue', 'eventDate', 'inHandDate', 'quantity', 'ticketId', 'stockType', 'notes', 'hasBarcodes',
         'electronicTransfer']]

#API call for Ticket Network Direct Listings
url = "https://www.tn-apis.com/inventory/v4/ticketgroups?perPage=500&page="
headers = {'X-Identity-Context': "broker-id=1368", 'Authorization': "Bearer 6d11d008-e91a-3c68-a813-17046c69fb61",
    'cache-control': "no-cache", 'Postman-Token': "ae729026-4261-471e-852f-dbd8b458a0a6"}
data = []
eventName = []
venueName = []
eventDate = []
quantity = []
ticketType = []
typeNotes = []
pages = [str(i) for i in range(1,151)]
for page in pages:
    response = get("https://www.tn-apis.com/inventory/v4/ticketgroups?perPage=500&page=" + page, headers = headers )
    response = response.text
    d2 = json.loads(response)
    data.extend(d2['results'])
    for i in d2['results']:
        try:
            eventName.append(i['event']['name'])
        except:
            eventName.append('NA')
        try:
            venueName.append(i['event']['venue']['name'])
        except:
            venueName.append('NA')
        try:
            eventDate.append(i['event']['date'])
        except:
            eventDate.append('NA')
        try:
            quantity.append(i['quantity']['total'])
        except:
            quantity.append('NA')
        try:
            ticketType.append(i['stockType']['description'])
        except:
            ticketType.append('NA')
        try:
            typeNotes.append(i['notes']['additional'])
        except:
            typeNotes.append('NA')
TN = pd.DataFrame(data)
TNcolumns = ['_links', 'broadcastChannelIds', 'brokerId', 'created', 'event', 'exchangeTicketGroupId',
             'hasBarcodes', 'hasPurchaseOrder', 'hasQrScreenshots', 'hideSeats', 'isInstant', 'isMercury', 'isOnHand',
             'isShort', 'isTNPrime', 'lastUpdatedBy', 'nearTerm', 'notes', 'orders', 'orders', 'purchaseOrderIds',
             'referenceTicketGroupId', 'seats', 'splitRule', 'stockType', 'tags', 'ticketGroupType', 'tickets',
             'updated', 'view', 'zone', 'pending']
TN.drop(TNcolumns, axis=1, inplace=True)
TN['eventName'] = eventName
TN['venueName'] = venueName
TN['eventDate'] = eventDate
TN['quantity'] = quantity
TN['ticketType'] = ticketType
TN['typeNotes'] = typeNotes
TN = TN[['eventName', 'venueName', 'eventDate', 'onHandDate', 'quantity', 'ticketGroupId', 'ticketType', 'typeNotes']]
#TN.to_csv('C:/outputfiles/TN_Raw.csv', index = False)

#Database Pull for StubHub Listings
def data_pull(server='192.168.10.209', database='STG', username='appuser', password='RetroG@mes', query=''):
    cnxn = pyodbc.connect('DRIVER={ODBC Driver 13 for SQL Server};SERVER='+server+';DATABASE='+database+';UID='+username
                          + ';PWD=' + password)
    return pd.read_sql_query(query, cnxn)
SH_TicketType_query = """SELECT [eventdescription] AS eventName
      ,[eventdate] AS eventDate
      ,[inhanddate] AS inHandDate
      ,[quantity]
      ,[venuedescription] AS venueName
      ,[deliveryoption] AS deliveryOption
      ,[fulfillmentMethodDisplayName]
      ,[internalNotes]
	  ,[listingid] AS listingId
      ,[externallistingid] AS externalListingId
  FROM [STG].[api].[StubHubListings]
  WHERE eventdate > GETDATE()"""
SH = data_pull(server='192.168.10.209', query=SH_TicketType_query)
#SH.to_csv('C:/outputfiles/SH_Raw.csv', index=False)

#API Call Using C# Program for Ticketmaster Listings
os.startfile("F:/TB-Applications/Tableau/listingtickettypesoutputfiles/Ticketmaster API Listing/bin/Debug/Ticketmaster API Listing.exe")
time.sleep(400)
TM = pd.read_csv('F:/TB-Applications/Tableau/listingtickettypesoutputfiles/inventories.csv', names=["eventName","eventDate", "eventTime", "inHandDate", "productionId",
                                                  "supplierTicketId", "cost", "description", "venueName"])
TM.to_csv('F:/TB-Applications/Tableau/listingtickettypesoutputfiles/TMinv.csv', index=False)
TM = pd.read_csv('F:/TB-Applications/Tableau/listingtickettypesoutputfiles/TMinv.csv')

#Identify StubHub Ticket Types
SH = SH.merge(TBType, how='left', on = 'externalListingId')
SH['internalNotes'] = SH['internalNotes'].astype(str)
def f(SH):
    if SH['fulfillmentMethodDisplayName'] == 'UPS' and SH['internalNotes'] == '.':
        return 'Mail'
    elif SH['fulfillmentMethodDisplayName'] == 'LMS' and 'Session' in SH['internalNotes']:
        return 'Mail'
    elif SH['fulfillmentMethodDisplayName'] == 'Barcode' and 'Delivery week' in SH['internalNotes']:
        return 'Mail'
    elif SH['fulfillmentMethodDisplayName'] == 'UPS' and 'Access' in SH['internalNotes']:
        return 'Mail'
    elif SH['fulfillmentMethodDisplayName'] == 'UPS' and 'pass' in SH['internalNotes']:
        return 'Mail'
    elif SH['fulfillmentMethodDisplayName'] == 'UPS' and 'admission' in SH['internalNotes']:
        return 'Mail'
    elif SH['fulfillmentMethodDisplayName'] == 'UPS' and 'General' in SH['internalNotes']:
        return 'Mail'
    elif SH['fulfillmentMethodDisplayName'] == 'UPS' and 'Delivery' in SH['internalNotes']:
        return 'Mail'
    elif SH['fulfillmentMethodDisplayName'] == 'Mobile' and 'MobileQR' in SH['internalNotes']:
        return 'Barcode'
    elif SH['fulfillmentMethodDisplayName'] == 'ExternalFlashTransfer' and 'FLS' in SH['internalNotes']:
        return 'Flash'
    elif SH['fulfillmentMethodDisplayName'] == 'Barcode' and 'XFER Mobile' in SH['internalNotes']:
        return 'Mobile'
    elif SH['fulfillmentMethodDisplayName'] == 'Mobile' and 'E-Delivery' in SH['internalNotes']:
        return 'Mobile'
    elif SH['fulfillmentMethodDisplayName'] == 'ExternalMobileTransfer' and 'XFER Mobile' in SH['internalNotes']:
        return 'Mobile'
    elif SH['fulfillmentMethodDisplayName'] == 'Mobile' and 'XFER Mobile' in SH['internalNotes']:
        return 'Mobile'
    elif SH['fulfillmentMethodDisplayName'] == 'Barcode' and 'MobileQR' in SH['internalNotes']:
        return 'Mobile'
    elif SH['fulfillmentMethodDisplayName'] == 'Mobile' and 'Mobile QR' in SH['internalNotes']:
        return 'Mobile'
    elif SH['fulfillmentMethodDisplayName'] == 'ExternalMobileTransfer' and 'Mobile QR' in SH['internalNotes']:
        return 'Mobile'
    elif SH['fulfillmentMethodDisplayName'] == 'Barcode' and 'E-Delivery' in SH['internalNotes']:
        return 'TicketFast'
    elif SH['fulfillmentMethodDisplayName'] == 'UPS' and 'E-Delivery' in SH['internalNotes']:
        return 'TicketFast'
    elif SH['fulfillmentMethodDisplayName'] == 'Barcode' and 'Food' in SH['internalNotes']:
        return 'TicketFast'
    elif SH['fulfillmentMethodDisplayName'] == 'PDF' and 'E-Delivery' in SH['internalNotes']:
        return 'TicketFast'
    elif SH['fulfillmentMethodDisplayName'] == 'PDF' and 'General' in SH['internalNotes']:
        return 'TicketFast'
    elif SH['fulfillmentMethodDisplayName'] == 'PDF' and 'Session' in SH['internalNotes']:
        return 'TicketFast'
    elif SH['fulfillmentMethodDisplayName'] == 'PDF' and '.' in SH['internalNotes']:
        return 'TicketFast'
    else:
        return SH['deliveryOption']
SH['Exchange Ticket Type'] = SH.apply(f, axis=1)
SH['Exchange Ticket Type'] = SH['Exchange Ticket Type'].replace(['BARCODE', 'EXTERNAL_TRANSFER', 'FLASHSEAT',
          'MOBILE_TICKET', 'PDF', 'UPS', 'LMS'],['Barcode', 'Mobile', 'Flash', 'Mobile', 'TicketFast', 'Mail', 'Mail'])
SHcols = ['fulfillmentMethodDisplayName', 'internalNotes', 'listingId', 'eventName_y', 'eventDate_y', 'quantity_y',
          'inHandDate_y', 'description', 'internalNote']
SH.drop(SHcols, axis=1, inplace=True)
SH = SH[['TB Ticket Type', 'Exchange Ticket Type', 'externalListingId', 'eventName_x', 'eventDate_x', 'venueName',
         'quantity_x', 'inHandDate_x', 'deliveryOption']]
SH = SH.rename(index=str, columns={"eventName_x":"eventName", "eventDate_x":"eventDate", "quantity_x":"quantity",
                                   "inHandDate_x":"inHandDate"})

#Identify Vivid Seats Ticket Types
VS = VS.rename(index=str, columns={"ticketId":"externalListingId"})
VS['externalListingId'] = VS['externalListingId'].astype(int)
VS = VS.merge(TBType, how='left', on = 'externalListingId')
def f(VS):
    if VS['stockType'] == 'ELECTRONIC' and 'QR' in VS['notes']:
        return 'Mobile'
    elif VS['stockType'] == 'ELECTRONIC' and VS['electronicTransfer'] == 'TRUE':
        return 'TicketFast'
    elif VS['stockType'] == 'FLASH':
        return 'Flash'
    elif VS['stockType'] == 'HARD':
        return 'Mail'
    elif VS['hasBarcodes'] == 'TRUE':
        return 'Barcode'
    elif VS['stockType'] == 'MOBILE_SCREENCAP':
        return 'Mobile'
    elif VS['stockType'] == 'TMET' and 'XFER' in VS['notes']:
        return 'Mobile'
    elif VS['stockType'] == 'TMET' and 'QR' in VS['notes']:
        return 'Mobile'
    else:
        return VS['stockType']
VS['Exchange Ticket Type'] = VS.apply(f, axis=1)
VS['Exchange Ticket Type'] = VS['Exchange Ticket Type'].replace(['ELECTRONIC'],['TicketFast'])
VScols = ['eventName_y', 'eventDate_y', 'quantity_y', 'description', 'inHandDate_y', 'internalNote', 'hasBarcodes',
          'electronicTransfer', 'notes']
VS.drop(VScols, axis=1, inplace=True)
VS = VS[['TB Ticket Type', 'Exchange Ticket Type', 'externalListingId', 'eventName_x', 'eventDate_x', 'venue',
         'quantity_x', 'inHandDate_x', 'stockType']]
VS = VS.rename(index=str, columns={"eventName_x":"eventName", "eventDate_x":"eventDate", "quantity_x":"quantity",
                                   "inHandDate_x":"inHandDate", "venue":"venueName"})

#Identify Ticket Network Direct Ticket Types
TN = TN.rename(index=str, columns={"ticketGroupId":"externalListingId"})
TN = TN.merge(TBType, how='left', on = 'externalListingId')
TN['Exchange Ticket Type'] = TN.ticketType
TN['Exchange Ticket Type'] = TN['Exchange Ticket Type'].replace(['Mobile', 'Standard', 'e-Ticket',
                                      'Flash Seats'],['Mobile', 'Mail', 'TicketFast', 'Flash'])
TNcols = ['eventName_y', 'eventDate_y', 'quantity_y', 'inHandDate', 'description', 'internalNote']
TN.drop(TNcols, axis=1, inplace=True)
TN = TN[['TB Ticket Type', 'Exchange Ticket Type', 'externalListingId', 'eventName_x', 'eventDate_x', 'venueName',
         'quantity_x', 'onHandDate', 'typeNotes']]
TN = TN.rename(index=str, columns={"eventName_x":"eventName", "eventDate_x":"eventDate", "quantity_x":"quantity"})

#Identify Ticketmaster ticket types
TM = TM.rename(index=str, columns={"supplierTicketId":"externalListingId"})
TM['externalListingId'] = TM['externalListingId'].astype(int)
TM = TM.merge(TBType, how='left', on = 'externalListingId')
TM['description_x'] = TM['description_x'].astype(str)
def f(TM):
    if 'Flash' in TM['description_x']:
        return 'Flash'
    elif 'XFER Mobile' in TM['description_x']:
        return 'Mobile'
    elif 'E-Delivery' in TM['description_x']:
        return 'TicketFast'
    elif 'Delivery' in TM['description_x']:
        return 'Mail'
    elif 'UPS' in TM['description_x']:
        return 'Mail'
    elif 'MobileQR' in TM['description_x']:
        return 'Barcode'
    else:
        return TM['description_x']
TM['Exchange Ticket Type'] = TM.apply(f, axis=1)
TM = TM[['TB Ticket Type', 'Exchange Ticket Type', 'eventName_x', 'eventDate_x', 'eventTime', 'venueName', 'productionId',
         'inHandDate_x', 'quantity', 'description_x', 'externalListingId']]
TM = TM.rename(index=str, columns={"eventName_x":"eventName", "eventDate_x":"eventDate", "description_x":"description",
                                   "inHandDate_x":"inHandDate"})

#Remove observations that are not discrepancies for each exchange
def f(SH):
   return 'yes' if SH['TB Ticket Type'] == SH['Exchange Ticket Type'] else 'no'
SH['Check'] = SH.apply(f, axis=1)
SH = SH[SH['Check'] == 'no']
SH.drop(['Check'], axis=1, inplace=True)

def f(VS):
   return 'yes' if VS['TB Ticket Type'] == VS['Exchange Ticket Type'] else 'no'
VS['Check'] = VS.apply(f, axis=1)
VS = VS[VS['Check'] == 'no']
VS.drop(['Check'], axis=1, inplace=True)

def f(TN):
   return 'yes' if TN['TB Ticket Type'] == TN['Exchange Ticket Type'] else 'no'
TN['Check'] = TN.apply(f, axis=1)
TN = TN[TN['Check'] == 'no']
TN.drop(['Check'], axis=1, inplace=True)

def f(TM):
   return 'yes' if TM['TB Ticket Type'] == TM['Exchange Ticket Type'] else 'no'
TM['Check'] = TM.apply(f, axis=1)
TM = TM[TM['Check'] == 'no']
TM.drop(['Check'], axis=1, inplace=True)

#Export dataframes to sheets in an excel workbook
writer = pd.ExcelWriter('F:/TB-Applications/Tableau/TicketTypes/ListingTicketTypes.xlsx', engine = 'xlsxwriter')
SH.to_excel(writer, sheet_name = 'StubHub', index = False)
TN.to_excel(writer, sheet_name = 'Ticket Network Direct', index = False)
VS.to_excel(writer, sheet_name = 'VividSeats', index = False)
TM.to_excel(writer, sheet_name = 'Ticketmaster', index = False)
workbook = writer.book
worksheet1 = writer.sheets['StubHub']
worksheet2 = writer.sheets['Ticket Network Direct']
worksheet3 = writer.sheets['VividSeats']
worksheet4 = writer.sheets['Ticketmaster']
worksheet1.set_column('A:I', 20)
worksheet2.set_column('A:I', 20)
worksheet3.set_column('A:I', 20)
worksheet4.set_column('A:I', 20)
writer.save()
print("Report Complete -- Close Window")
sys.exit()