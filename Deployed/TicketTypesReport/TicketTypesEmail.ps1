$fromaddress = "noreply@ticketboat.com" 
$toaddress = "<apierson@ticketboat.com>, <ckesner@ticketboat.com>, <lindsay@ticketboat.com>, <lgregerson@ticketboat.com>" 
$Subject = "Ticket Types Reports" 
$body = "Please find the ticket type reports attached for current orders and listings."
$smtpserver = "smtp.office365.com" 

$message = new-object System.Net.Mail.MailMessage 
$message.From = $fromaddress 
$message.To.Add($toaddress)
$message.IsBodyHtml = $True 
$message.Subject = $Subject

#your file location
$files=Get-ChildItem "F:\TB-Applications\Data\Deployed\TicketTypesReport\TicketTypes"
Foreach($file in $files)
{
Write-Host �Attaching File :- � $file
$attachment = new-object System.Net.Mail.Attachment -ArgumentList $file.FullName
$message.Attachments.Add($attachment)
}

$message.body = $body 
$smtp = new-object Net.Mail.SmtpClient($smtpserver, 587)
$smtp.EnableSsl = $true 
$smtp.Credentials = New-Object System.Net.NetworkCredential(�noreply@ticketboat.com�, �RetroG@mes�);  
$smtp.Send($message)