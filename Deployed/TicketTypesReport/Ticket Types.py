#Read in libraries
import re
import os
import csv
import json
import glob
import pyodbc
import smtplib
import requests
import http.client
import numpy as np
import pandas as pd
from requests import get
from datetime import datetime
from datetime import timedelta
import xml.etree.ElementTree as ET
from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart
from email.mime.application import MIMEApplication

#Define the function for pulling data from SQL Server database
def data_pull(server='192.168.10.203', database='eiboxoffice', username='appuser', password='RetroG@mes', query=''):
    cnxn = pyodbc.connect('DRIVER={ODBC Driver 13 for SQL Server};SERVER='+server+';DATABASE='+database+';UID='+username
                          + ';PWD=' + password)
    return pd.read_sql_query(query, cnxn)

# #Define query to pull NIH and empty shipping info
TB_TicketType_query = """SELECT SV.InternalNote
	  ,SO.ExternalReferenceNumber
      ,SV.EventName
	  ,SV.EventDate
	  ,SV.VenueName
      ,SO.FinalizedDateTime
      ,SV.IsCancelled
  FROM eiboxoffice.dbo.TB_SalesView_v2 AS SV
  LEFT JOIN eiboxoffice.dbo.SellOrder AS SO ON SO.InvoiceNumber = SV.InvoiceNumber
  WHERE SO.ShippingTrackingCode LIKE '%NIH%'
  AND (SO.BillingAttn LIKE '%stub%' OR SO.BillingAttn LIKE '%Ticketmaster%' OR SO.BillingAttn LIKE '%Vivid%' OR SO.BillingAttn LIKE '%TND%')
  AND SV.EventDate >= GETDATE()
"""

TBType = data_pull(server='192.168.10.203', query=TB_TicketType_query)
TBType.to_csv('F:/TB-Applications/Data/Deployed/TicketTypesReport/outputfiles/TBType.csv', index = False)
TBType = pd.read_csv('F:/TB-Applications/Data/Deployed/TicketTypesReport/outputfiles/TBType.csv')

TBType.loc[TBType.InternalNote.str.contains("Ships|SHIPS|@ST:1 @SM:2|@ST:4 @SM:11|@ST:4 @SM:12|@ST:1 @SM:3|HS", na=False), 'TB Ticket Type'] = 'Mail'
TBType.loc[TBType.InternalNote.str.contains("TF|CBE|tF|Tf|PDF", na=False), 'TB Ticket Type'] = 'TicketFast'
TBType.loc[TBType.InternalNote.str.contains("@ST:7 @SM:13|XFER", na=False), 'TB Ticket Type'] = 'Mobile'
TBType.loc[TBType.InternalNote.str.contains("@ST:5 @SM:15|FLS|Flash|ST:5 @SM:15", na=False), 'TB Ticket Type'] = 'Flash'
TBType.loc[TBType.InternalNote.str.contains("@ST:7 @SM:14", na=False), 'TB Ticket Type'] = 'Barcode'


#API call for Ticket Network Direct Listings
headers = {'X-Identity-Context': "broker-id=1368", 'Authorization': "Bearer 6d11d008-e91a-3c68-a813-17046c69fb61",
    'cache-control': "no-cache", 'Postman-Token': "ae729026-4261-471e-852f-dbd8b458a0a6"}
data = []
ticketGroupNotes = []
requestedDeliveryMethod = []
eventDate = []
eventName = []
eventTime =[]
venueName =[]
pages = [str(i) for i in range(0,15)]
for page in pages:
    response = get("https://www.tn-apis.com/order/v4/orders?$filter=delivery/status/id eq 1&perPage=500&page=" + page, headers = headers )
    response = response.text
    d2 = json.loads(response)
    data.extend(d2['results'])
    for i in d2['results']:
        try:
            requestedDeliveryMethod.append(i['ticketRequest']['requestedDeliveryMethod']['description'])
        except:
            requestedDeliveryMethod.append('NA')
        try:
            ticketGroupNotes.append(i['ticketRequest']['ticketGroupNotes'])
        except:
            ticketGroupNotes.append('NA')
        try:
            eventDate.append(i['soldTicketGroups'][0]['event']['date'])
        except:
            eventDate.append('NA')
        try:
            eventName.append(i['soldTicketGroups'][0]['event']['name'])
        except:
            eventName.append('NA')
        try:
            eventTime.append(i['soldTicketGroups'][0]['event']['time'])
        except:
            eventTime.append('NA')
        try:
            venueName.append(i['soldTicketGroups'][0]['event']['venue']['name'])
        except:
            venueName.append('NA')
TNDPD = pd.DataFrame(data)
TNcolumns = ['_links', 'date', 'customer', 'createdBy', 'delivery', 'discounts', 'fees', 'hasETickets',
             'hasProofOfDelivery', 'hasQrScreenshots', 'hasTransferUrl', 'id', 'invoice', 'isShort',
             'mercuryTransactionId', 'payment', 'purchaseOrderIds', 'requestedTicketGroups', 'saleDate',
             'soldTicketGroups', 'status', 'tags', 'ticketRequest', 'ticketsLocked', 'total', 'type', 'voidDate',
             'voidNote']
TNDPD.drop(TNcolumns, axis=1, inplace=True)
TNDPD['requestedDeliveryMethodDescription'] = requestedDeliveryMethod
TNDPD['ticketGroupNotes'] = ticketGroupNotes
TNDPD['eventDate'] = eventDate
TNDPD['eventName'] = eventName
TNDPD['eventTime'] = eventTime
TNDPD['venueName'] = venueName
TNDPD = TNDPD[['eventName', 'eventDate', 'eventTime', 'venueName', 'onHandDate', 'ticketQuantity', 'ticketGroupNotes',
               'requestedDeliveryMethodDescription', 'referenceId']]
TNDPD = TNDPD[TNDPD.eventDate != 'NA']


#API call for VividSeats
conn = http.client.HTTPSConnection("brokers.vividseats.com")

headers = {
    'Cache-Control': "no-cache",
    'Postman-Token': "a6877d71-dc0f-4971-a10f-4b63a55c227a"
    }

V = conn.request("GET",
                 "/webservices/v1/getOrders?apiToken=2493e445-e5c2-41f0-9d84-a7a6c9b8c238&status=PENDING_SHIPMENT",
                 headers=headers)

res = conn.getresponse()
VSUF = res.read()
VSUF = VSUF.decode("utf-8")
print(VSUF)


class XML2DataFrame:

    def __init__(self, xml_data):
        self.root = ET.XML(xml_data)

    def parse_root(self, root):
        return [self.parse_element(child) for child in iter(root)]

    def parse_element(self, element, parsed=None):
        if parsed is None:
            parsed = dict()
        for key in element.keys():
            parsed[key] = element.attrib.get(key)
        if element.text:
            parsed[element.tag] = element.text
        for child in list(element):
            self.parse_element(child, parsed)
        return parsed

    def process_data(self):
        structure_data = self.parse_root(self.root)
        return pd.DataFrame(structure_data)


xml2df = XML2DataFrame(VSUF)
xml_dataframe = xml2df.process_data()
xml_dataframe.to_csv('F:/TB-Applications/Data/Deployed/TicketTypesReport/outputfiles/VSUS.csv', index=False)
VSUS = pd.read_csv('F:/TB-Applications/Data/Deployed/TicketTypesReport/outputfiles/VSUS.csv')

#API call for Ticketmaster
url = "https://api.eventinventory.com/BrokerAPI/V1/Orders/"
querystring = {"securityToken":"21775A047A9F4227B71711E6E7FB658B","brokerId":"1395","orderStatuses":"10,101"}
payload = ""
headers = {
    'cache-control': "no-cache",
    'Postman-Token': "1f745628-015e-448f-995f-8edf544d54a3"
    }
response = requests.request("GET", url, data=payload, headers=headers, params=querystring)
response = response.content.decode("utf-8")

column_headers = []
root = ET.fromstring(response)

for i in root[1][0]:
    column_headers.append(re.sub(r"\s*{.*}\s*", " ", i.tag))

with open('F:/TB-Applications/Data/Deployed/TicketTypesReport/outputfiles/TMRTF.csv', 'w', newline='') as file:
    writer = csv.writer(file)
    writer.writerow(column_headers)
    for i in root[1]:
        temp = []
        for x in i:
            temp.append(x.text)
        writer.writerow(temp)

file.close()
TMRTF = pd.read_csv('F:/TB-Applications/Data/Deployed/TicketTypesReport/outputfiles/TMRTF.csv', encoding='cp1252')

#API call for StubHub
def data_pull(server='192.168.10.209', database='STG', username='appuser', password='RetroG@mes', query=''):
    cnxn = pyodbc.connect('DRIVER={ODBC Driver 13 for SQL Server};SERVER='+server+';DATABASE='+database+';UID='+username+';PWD='+ password)
    return pd.read_sql_query(query, cnxn)

token_query = 'SELECT * FROM [STG].[api].[tokens]'
token = data_pull(server='192.168.10.209', query=token_query).loc[2].value


conn = http.client.HTTPSConnection("api.stubhub.com")

headers = {
    'Authorization': "Bearer " + token,
    'Cache-Control': "no-cache",
    'Postman-Token': "5d0fb3d0-43e9-4c17-825f-6679bd0a3756"
    }

def api_call(max_length_per_call):
    req = conn.request("GET",
            "/accountmanagement/sales/v1/seller/6C21FF9F4E1D3BC0E04400144FB7AAA6?filters=status:CONFIRMED&start="
                       +str(max_length_per_call)+"&end="+str(max_length_per_call + 250), headers=headers)
    res = conn.getresponse()
    data = res.read()
    data = data.decode("utf-8")
    data = pd.read_json(data, orient = 'index')
    return data

sales = api_call(0)['sale']

for i in range(1,12):
    sales += api_call((i*250)+1)['sale']

sales.to_json('F:/TB-Applications/Data/Deployed/TicketTypesReport/outputfiles/SHUF.json', orient='index')
df = pd.read_json('F:/TB-Applications/Data/Deployed/TicketTypesReport/outputfiles/SHUF.json')


toCSV = df['sales']
keys = toCSV[0].keys()
with open('F:/TB-Applications/Data/Deployed/TicketTypesReport/outputfiles/SHUF.csv', 'w', newline = "") as output_file:
    dict_writer = csv.DictWriter(output_file, keys, extrasaction = 'ignore')
    dict_writer.writeheader()
    dict_writer.writerows(toCSV)


#Read in and filter StubHub data
SHUF = pd.read_csv('F:/TB-Applications/Data/Deployed/TicketTypesReport/outputfiles/SHUF.csv', encoding='cp1252')
SHUF['eventDate'] = [x.split('T')[0] for x in SHUF['eventDate']]
SHUF['eventDate'] = pd.to_datetime(SHUF['eventDate'])
yesterday = pd.datetime.today() - timedelta(1)
SHUF = SHUF.loc[SHUF['eventDate'] >= yesterday]
SHUF = SHUF.rename(index=str, columns={"saleId":"ExternalReferenceNumber"})
SHUF = SHUF.merge(TBType, how='left', on = 'ExternalReferenceNumber')
SHUF = SHUF[SHUF.IsCancelled != 1]

#Read in and filter VividSeats data
VSUS = pd.read_csv('F:/TB-Applications/Data/Deployed/TicketTypesReport/outputfiles/VSUS.csv')
VSUS['eventDate'] = pd.to_datetime(VSUS['eventDate'])
VSUS = VSUS.loc[VSUS['eventDate'] >= yesterday]
VSUS = VSUS.rename(index=str, columns={"orderId":"ExternalReferenceNumber"})
VSUS = VSUS.merge(TBType, how='left', on = 'ExternalReferenceNumber')
VSUS = VSUS[VSUS.IsCancelled != 1]

#Read in and filter Ticketmaster data
TMRTF = pd.read_csv('F:/TB-Applications/Data/Deployed/TicketTypesReport/outputfiles/TMRTF.csv', encoding='cp1252')
TMRTF[' EventDate'] = [x.split('T')[0] for x in TMRTF[' EventDate']]
TMRTF[' EventDate'] = pd.to_datetime(TMRTF[' EventDate'])
TMRTF = TMRTF.loc[TMRTF[' EventDate'] >= yesterday]
TMRTF = TMRTF.rename(index=str, columns={" PurchaseOrderNumber":"ExternalReferenceNumber"})
TMRTF = TMRTF.merge(TBType, how='left', on = 'ExternalReferenceNumber')
TMRTF = TMRTF[TMRTF.IsCancelled != 1]

#Read in and filter Ticket Network Direct data
TNDPD['referenceId'] = TNDPD['referenceId'].astype(int)
TNDPD = TNDPD.rename(index=str, columns={"referenceId":"ExternalReferenceNumber"})
TNDPD = TNDPD.merge(TBType, how='left', on = 'ExternalReferenceNumber')
TNDPD = TNDPD[TNDPD.IsCancelled != 1]
TNDPD = TNDPD.rename(index=str, columns={"eventdate_x":"eventdate"})
TNDPD['eventDate'] = pd.to_datetime(TNDPD['eventDate'])
TNDPD = TNDPD.loc[TNDPD['eventDate'] >= yesterday]

#Reorganize column order, rename headers, replace boolean values with shipped status
TNDPD['Exchange Ticket Type'] = TNDPD.requestedDeliveryMethodDescription
TNDPD = TNDPD[['TB Ticket Type', 'Exchange Ticket Type', 'ExternalReferenceNumber', 'ticketQuantity', 'ticketQuantity', 'eventDate',
               'onHandDate', 'requestedDeliveryMethodDescription', 'IsCancelled']]
TNDPD['Exchange Ticket Type'] = TNDPD['requestedDeliveryMethodDescription'].replace(['Direct Transfer', 'Electronic Transfer', 'E-Ticket', 'FedEx Express Saver', 'Paperless', 'Flash Transfer'],['Mobile', 'Mobile', 'TicketFast', 'Mail', 'Flash', 'Flash'])

#Identify StubHub ticket types
SHUF['internalNotes'] = SHUF['internalNotes'].astype(str)

def f(SHUF):
    if SHUF['fulfillmentMethodDisplayName'] == 'UPS' and SHUF['internalNotes'] == '.':
        return 'Mail'
    elif SHUF['fulfillmentMethodDisplayName'] == 'LMS' and 'Session' in SHUF['internalNotes']:
        return 'Mail'
    elif SHUF['fulfillmentMethodDisplayName'] == 'Barcode' and 'Delivery week' in SHUF['internalNotes']:
        return 'Mail'
    elif SHUF['fulfillmentMethodDisplayName'] == 'UPS' and 'Access' in SHUF['internalNotes']:
        return 'Mail'
    elif SHUF['fulfillmentMethodDisplayName'] == 'UPS' and 'pass' in SHUF['internalNotes']:
        return 'Mail'
    elif SHUF['fulfillmentMethodDisplayName'] == 'UPS' and 'admission' in SHUF['internalNotes']:
        return 'Mail'
    elif SHUF['fulfillmentMethodDisplayName'] == 'UPS' and 'General' in SHUF['internalNotes']:
        return 'Mail'
    elif SHUF['fulfillmentMethodDisplayName'] == 'UPS' and 'Delivery' in SHUF['internalNotes']:
        return 'Mail'
    elif SHUF['fulfillmentMethodDisplayName'] == 'Mobile' and 'MobileQR' in SHUF['internalNotes']:
        return 'Barcode'
    elif SHUF['fulfillmentMethodDisplayName'] == 'ExternalFlashTransfer' and 'FLS' in SHUF['internalNotes']:
        return 'Flash'
    elif SHUF['fulfillmentMethodDisplayName'] == 'Barcode' and 'XFER Mobile' in SHUF['internalNotes']:
        return 'Mobile'
    elif SHUF['fulfillmentMethodDisplayName'] == 'Mobile' and 'E-Delivery' in SHUF['internalNotes']:
        return 'Mobile'
    elif SHUF['fulfillmentMethodDisplayName'] == 'ExternalMobileTransfer' and 'XFER Mobile' in SHUF['internalNotes']:
        return 'Mobile'
    elif SHUF['fulfillmentMethodDisplayName'] == 'Mobile' and 'XFER Mobile' in SHUF['internalNotes']:
        return 'Mobile'
    elif SHUF['fulfillmentMethodDisplayName'] == 'Barcode' and 'MobileQR' in SHUF['internalNotes']:
        return 'Mobile'
    elif SHUF['fulfillmentMethodDisplayName'] == 'Mobile' and 'Mobile QR' in SHUF['internalNotes']:
        return 'Mobile'
    elif SHUF['fulfillmentMethodDisplayName'] == 'ExternalMobileTransfer' and 'Mobile QR' in SHUF['internalNotes']:
        return 'Mobile'
    elif SHUF['fulfillmentMethodDisplayName'] == 'Barcode' and 'E-Delivery' in SHUF['internalNotes']:
        return 'TicketFast'
    elif SHUF['fulfillmentMethodDisplayName'] == 'UPS' and 'E-Delivery' in SHUF['internalNotes']:
        return 'TicketFast'
    elif SHUF['fulfillmentMethodDisplayName'] == 'Barcode' and 'Food' in SHUF['internalNotes']:
        return 'TicketFast'
    elif SHUF['fulfillmentMethodDisplayName'] == 'PDF' and 'E-Delivery' in SHUF['internalNotes']:
        return 'TicketFast'
    elif SHUF['fulfillmentMethodDisplayName'] == 'PDF' and 'General' in SHUF['internalNotes']:
        return 'TicketFast'
    elif SHUF['fulfillmentMethodDisplayName'] == 'PDF' and 'Session' in SHUF['internalNotes']:
        return 'TicketFast'
    elif SHUF['fulfillmentMethodDisplayName'] == 'PDF' and '.' in SHUF['internalNotes']:
        return 'TicketFast'
    else:
        return SHUF['deliveryOption']
SHUF['Exchange Ticket Type'] = SHUF.apply(f, axis=1)

SHUF['Exchange Ticket Type'] = SHUF['Exchange Ticket Type'].replace(['BARCODE', 'EXTERNAL_TRANSFER', 'FLASHSEAT', 'MOBILE_TICKET', 'PDF', 'UPS', 'LMS'],['Barcode', 'Mobile', 'Flash', 'Mobile', 'TicketFast', 'Mail', 'Mail'])
SHUF = SHUF[['TB Ticket Type', 'Exchange Ticket Type', 'ExternalReferenceNumber', 'eventId', 'eventDescription', 'venueDescription', 'quantity',
             'eventDate', 'eventTimeZone', 'inhandDate', 'section', 'rows', 'seats', 'saleDate', 'internalNotes',
             'fulfillmentMethodDisplayName', 'inHand', 'city', 'state', 'buyerId', 'ga', 'externalTransfer', 'deliveryOption']]

#Identify Ticketmaster ticket types
TMRTF[' ExternalNotes'] = TMRTF[' ExternalNotes'].astype(str)

def f(TMRTF):
    if TMRTF[' ShippingMethodName'] == 'Mobile e-Ticket' and 'Flash' in TMRTF[' ExternalNotes']:
        return 'Flash'
    elif 'UPS' in TMRTF[' ShippingMethodName'] and '' in TMRTF[' ExternalNotes']:
        return 'Mail'
    elif TMRTF[' ShippingMethodName'] == 'Electronic Delivery' and 'E-Delivery' in TMRTF[' ExternalNotes']:
        return 'TicketFast'
    else:
        return TMRTF[' ShippingMethodName']
TMRTF['Exchange Ticket Type'] = TMRTF.apply(f, axis=1)

TMRTF['Exchange Ticket Type'] = TMRTF['Exchange Ticket Type'].replace(['Electronic Delivery', 'Mobile e-Ticket', 'Ticketmaster'+u'\xae'+' Verified - Electronic Delivery', 'UPS Basic Delivery', 'UPS One Day Shipping'],['Flash', 'Mobile', 'TicketFast', 'Mail', 'Mail'])

TMRTF = TMRTF[['TB Ticket Type', 'Exchange Ticket Type', 'ExternalReferenceNumber', ' EventName', ' VenueName', ' StartSeat', ' Row',
               ' Section', ' EndSeat', ' OrderDate', ' EventDate', ' InHandDate', ' Quantity', ' ShippingMethodName',
               ' TotalCost', ' ShippingMethodName', ' ExternalNotes']]
#Identify VividSeats ticket types
def VSType (row):
    if row['electronicDelivery'] == 1:
        return 'E'
    if row['electronicDelivery'] == 0:
        return 'NoE'
VSUS['VSType'] = VSUS.apply(VSType, axis=1)
VSUS['electronicDeliverynotes'] = VSUS['VSType'] + VSUS['notes'].astype(str)
VSUS.loc[VSUS.electronicDeliverynotes.str.contains("NoE18|NoE3|NoEClub|NoEFair|NoEGeneral|NoELimited|NoESaturday|NoEVIP|NoEnan", na=False), 'Exchange Ticket Type'] = 'Mail'
VSUS.loc[VSUS.electronicDeliverynotes.str.contains("E18|E21|E3|EFood|EGeneral|EObstructed|ERear|ESide|EStanding|Enan", na=False), 'Exchange Ticket Type'] = 'TicketFast'
VSUS.loc[VSUS.electronicDeliverynotes.str.contains("Flash|FLS", na=False), 'Exchange Ticket Type'] = 'Flash'
VSUS.loc[VSUS.electronicDeliverynotes.str.contains("iOS or Android|XFER|smartphone", na=False), 'Exchange Ticket Type'] = 'Mobile'
VSUS = VSUS[['TB Ticket Type', 'Exchange Ticket Type', 'ExternalReferenceNumber', 'event', 'eventDate', 'electronicDelivery', 'notes']]


#Remove observations that are not discrepancies for each exchange
def f(SHUF):
   return 'yes' if SHUF['TB Ticket Type'] == SHUF['Exchange Ticket Type'] else 'no'
SHUF['Check'] = SHUF.apply(f, axis=1)
SHUF = SHUF[SHUF['Check'] == 'no']
# SHUF.drop(SHUF['Check'])


def f(VSUS):
   return 'yes' if VSUS['TB Ticket Type'] == VSUS['Exchange Ticket Type'] else 'no'
VSUS['Check'] = VSUS.apply(f, axis=1)
VSUS = VSUS[VSUS['Check'] == 'no']
# SHUF.drop(SHUF['Check'])


def f(TMRTF):
   return 'yes' if TMRTF['TB Ticket Type'] == TMRTF['Exchange Ticket Type'] else 'no'
TMRTF['Check'] = TMRTF.apply(f, axis=1)
TMRTF = TMRTF[TMRTF['Check'] == 'no']
# TMRTF.drop(TMRTF['Check'])


def f(TNDPD):
   return 'yes' if TNDPD['TB Ticket Type'] == TNDPD['Exchange Ticket Type'] else 'no'
TNDPD['Check'] = TNDPD.apply(f, axis=1)
TNDPD = TNDPD[TNDPD['Check'] == 'no']
# TNDPD.drop(TNDPD['Check'])
# TNDPD = TNDPD.drop(TNDPD['Check'])


#Export dataframes to sheets in an excel workbook
writer = pd.ExcelWriter('F:/TB-Applications/Data/Deployed/TicketTypesReport/TicketTypes/OrderTicketTypes.xlsx', engine = 'xlsxwriter', )
SHUF.to_excel(writer, sheet_name = 'StubHub', index = False, encoding='utf-8')
TNDPD.to_excel(writer, sheet_name = 'Ticket Network Direct', index = False, encoding='utf-8')
TMRTF.to_excel(writer, sheet_name = 'Ticketmaster', index = False, encoding='utf-8')
VSUS.to_excel(writer, sheet_name = 'VividSeats', index = False, encoding='utf-8')
workbook = writer.book
worksheet1 = writer.sheets['StubHub']
worksheet2 = writer.sheets['Ticket Network Direct']
worksheet3 = writer.sheets['Ticketmaster']
worksheet4 = writer.sheets['VividSeats']
worksheet1.set_column('A:H', 20)
worksheet2.set_column('A:J', 20)
worksheet3.set_column('A:O', 20)
worksheet4.set_column('A:G', 20)
writer.save()

#Read in libraries
from requests import get
import pandas as pd
import requests
import pyodbc
import json
import time
import sys
import os

#Database Pull for Ticket Boat TicketGroups
def data_pull(server='192.168.10.203', database='eiboxoffice', username='appuser', password='RetroG@mes', query=''):
    cnxn = pyodbc.connect('DRIVER={ODBC Driver 13 for SQL Server};SERVER='+server+';DATABASE='+database+';UID='+username
                          + ';PWD=' + password)
    return pd.read_sql_query(query, cnxn)
TB_TicketType_query = """SELECT E.[EventName] AS eventName
	  ,TG.[TicketGroupID] AS externalListingId
      ,TG.[EventDate] AS eventDate
      ,TG.[Quantity] AS quantity
      ,TG.[InHandDate] AS inHandDate
      ,TG.[Description] AS description
      ,TG.[InternalNote] AS internalNote
  FROM [eiboxoffice].[dbo].[TicketGroup] TG
  LEFT JOIN [eiboxoffice].[dbo].[Event] E ON E.EventID = TG.[PrimaryEventID]
  LEFT JOIN [eiboxoffice].[dbo].[Venue] V ON V.VenueID = TG.[VenueID]
  WHERE TG.[EventDate] >= GETDATE()"""
TBType = data_pull(server='192.168.10.203', query=TB_TicketType_query)
TBType.loc[TBType.internalNote.str.contains("Ships|SHIPS|@ST:1 @SM:2|@ST:4 @SM:11|@ST:4 @SM:12|@ST:1 @SM:3|HS", na=False), 'TB Ticket Type'] = 'Mail'
TBType.loc[TBType.internalNote.str.contains("TF|CBE|tF|Tf|PDF", na=False), 'TB Ticket Type'] = 'TicketFast'
TBType.loc[TBType.internalNote.str.contains("@ST:7 @SM:13|XFER", na=False), 'TB Ticket Type'] = 'Mobile'
TBType.loc[TBType.internalNote.str.contains("@ST:5 @SM:15|FLS|Flash|ST:5 @SM:15", na=False), 'TB Ticket Type'] = 'Flash'
TBType.loc[TBType.internalNote.str.contains("@ST:7 @SM:14", na=False), 'TB Ticket Type'] = 'Barcode'
TBType.to_csv('F:/TB-Applications/Data/Deployed/TicketTypesReport/listingtickettypesoutputfiles/TBType.csv', index = False)

#API call for VividSeats Listings
url = "https://brokers.vividseats.com:443/webservices/listings/v2/get"
payload = ""
headers = {'Api-token': "2493e445-e5c2-41f0-9d84-a7a6c9b8c238",
           'cache-control': "no-cache", 'Postman-Token': "d49b9a77-465e-4a37-ba71-31d184f5c935"}
response = requests.request("GET", url, data=payload, headers=headers)
data = response.text
d2 = json.loads(data)
VS = pd.DataFrame(d2['listings'])
#VS.to_csv('C:/outputfiles/VS_Raw.csv', index=False)
VScolumns = ['attributes', 'city', 'cost', 'electronic', 'electronicRaw', 'electronicTransferRaw', 'faceValue',
             'hasFiles', 'instantDownload', 'internalNotes', 'instantDownload', 'lastUpdate', 'listDate',
             'passThrough', 'price', 'productionId', 'row', 'seatFrom', 'seatThru', 'section', 'splitType', 'splitValue',
             'state', 'tickets', 'spec']
VS.drop(VScolumns, axis=1, inplace=True)
VS = VS[['eventName', 'venue', 'eventDate', 'inHandDate', 'quantity', 'ticketId', 'stockType', 'notes', 'hasBarcodes',
         'electronicTransfer']]

#API call for Ticket Network Direct Listings
url = "https://www.tn-apis.com/inventory/v4/ticketgroups?perPage=500&page="
headers = {'X-Identity-Context': "broker-id=1368", 'Authorization': "Bearer 6d11d008-e91a-3c68-a813-17046c69fb61",
    'cache-control': "no-cache", 'Postman-Token': "ae729026-4261-471e-852f-dbd8b458a0a6"}
data = []
eventName = []
venueName = []
eventDate = []
quantity = []
ticketType = []
typeNotes = []
pages = [str(i) for i in range(1,151)]
for page in pages:
    response = get("https://www.tn-apis.com/inventory/v4/ticketgroups?perPage=500&page=" + page, headers = headers )
    response = response.text
    d2 = json.loads(response)
    data.extend(d2['results'])
    for i in d2['results']:
        try:
            eventName.append(i['event']['name'])
        except:
            eventName.append('NA')
        try:
            venueName.append(i['event']['venue']['name'])
        except:
            venueName.append('NA')
        try:
            eventDate.append(i['event']['date'])
        except:
            eventDate.append('NA')
        try:
            quantity.append(i['quantity']['total'])
        except:
            quantity.append('NA')
        try:
            ticketType.append(i['stockType']['description'])
        except:
            ticketType.append('NA')
        try:
            typeNotes.append(i['notes']['additional'])
        except:
            typeNotes.append('NA')
TN = pd.DataFrame(data)
TNcolumns = ['_links', 'broadcastChannelIds', 'brokerId', 'created', 'event', 'exchangeTicketGroupId',
             'hasBarcodes', 'hasPurchaseOrder', 'hasQrScreenshots', 'hideSeats', 'isInstant', 'isMercury', 'isOnHand',
             'isShort', 'isTNPrime', 'lastUpdatedBy', 'nearTerm', 'notes', 'orders', 'orders', 'purchaseOrderIds',
             'referenceTicketGroupId', 'seats', 'splitRule', 'stockType', 'tags', 'ticketGroupType', 'tickets',
             'updated', 'view', 'zone', 'pending']
TN.drop(TNcolumns, axis=1, inplace=True)
TN['eventName'] = eventName
TN['venueName'] = venueName
TN['eventDate'] = eventDate
TN['quantity'] = quantity
TN['ticketType'] = ticketType
TN['typeNotes'] = typeNotes
TN = TN[['eventName', 'venueName', 'eventDate', 'onHandDate', 'quantity', 'ticketGroupId', 'ticketType', 'typeNotes']]
#TN.to_csv('C:/outputfiles/TN_Raw.csv', index = False)

#Database Pull for StubHub Listings
def data_pull(server='192.168.10.209', database='STG', username='appuser', password='RetroG@mes', query=''):
    cnxn = pyodbc.connect('DRIVER={ODBC Driver 13 for SQL Server};SERVER='+server+';DATABASE='+database+';UID='+username
                          + ';PWD=' + password)
    return pd.read_sql_query(query, cnxn)
SH_TicketType_query = """SELECT [eventdescription] AS eventName
      ,[eventdate] AS eventDate
      ,[inhanddate] AS inHandDate
      ,[quantity]
      ,[venuedescription] AS venueName
      ,[deliveryoption] AS deliveryOption
      ,[fulfillmentMethodDisplayName]
      ,[internalNotes]
	  ,[listingid] AS listingId
      ,[externallistingid] AS externalListingId
  FROM [STG].[api].[StubHubListings]
  WHERE eventdate > GETDATE()"""
SH = data_pull(server='192.168.10.209', query=SH_TicketType_query)
#SH.to_csv('C:/outputfiles/SH_Raw.csv', index=False)

#API Call Using C# Program for Ticketmaster Listings
os.startfile("F:/TB-Applications/Data/Deployed/TicketTypesReport/listingtickettypesoutputfiles/Ticketmaster API Listing/bin/Debug/Ticketmaster API Listing.exe")
time.sleep(400)
TM = pd.read_csv('F:/TB-Applications/Data/Deployed/TicketTypesReport/listingtickettypesoutputfiles/inventories.csv', names=["eventName","eventDate", "eventTime", "inHandDate", "productionId",
                                                  "supplierTicketId", "cost", "description", "venueName"])
TM.to_csv('F:/TB-Applications/Data/Deployed/TicketTypesReport/listingtickettypesoutputfiles/TMinv.csv', index=False)
TM = pd.read_csv('F:/TB-Applications/Data/Deployed/TicketTypesReport/listingtickettypesoutputfiles/TMinv.csv')

#Identify StubHub Ticket Types
SH = SH.merge(TBType, how='left', on = 'externalListingId')
SH['internalNotes'] = SH['internalNotes'].astype(str)
def f(SH):
    if SH['fulfillmentMethodDisplayName'] == 'UPS' and SH['internalNotes'] == '.':
        return 'Mail'
    elif SH['fulfillmentMethodDisplayName'] == 'LMS' and 'Session' in SH['internalNotes']:
        return 'Mail'
    elif SH['fulfillmentMethodDisplayName'] == 'Barcode' and 'Delivery week' in SH['internalNotes']:
        return 'Mail'
    elif SH['fulfillmentMethodDisplayName'] == 'UPS' and 'Access' in SH['internalNotes']:
        return 'Mail'
    elif SH['fulfillmentMethodDisplayName'] == 'UPS' and 'pass' in SH['internalNotes']:
        return 'Mail'
    elif SH['fulfillmentMethodDisplayName'] == 'UPS' and 'admission' in SH['internalNotes']:
        return 'Mail'
    elif SH['fulfillmentMethodDisplayName'] == 'UPS' and 'General' in SH['internalNotes']:
        return 'Mail'
    elif SH['fulfillmentMethodDisplayName'] == 'UPS' and 'Delivery' in SH['internalNotes']:
        return 'Mail'
    elif SH['fulfillmentMethodDisplayName'] == 'Mobile' and 'MobileQR' in SH['internalNotes']:
        return 'Barcode'
    elif SH['fulfillmentMethodDisplayName'] == 'ExternalFlashTransfer' and 'FLS' in SH['internalNotes']:
        return 'Flash'
    elif SH['fulfillmentMethodDisplayName'] == 'Barcode' and 'XFER Mobile' in SH['internalNotes']:
        return 'Mobile'
    elif SH['fulfillmentMethodDisplayName'] == 'Mobile' and 'E-Delivery' in SH['internalNotes']:
        return 'Mobile'
    elif SH['fulfillmentMethodDisplayName'] == 'ExternalMobileTransfer' and 'XFER Mobile' in SH['internalNotes']:
        return 'Mobile'
    elif SH['fulfillmentMethodDisplayName'] == 'Mobile' and 'XFER Mobile' in SH['internalNotes']:
        return 'Mobile'
    elif SH['fulfillmentMethodDisplayName'] == 'Barcode' and 'MobileQR' in SH['internalNotes']:
        return 'Mobile'
    elif SH['fulfillmentMethodDisplayName'] == 'Mobile' and 'Mobile QR' in SH['internalNotes']:
        return 'Mobile'
    elif SH['fulfillmentMethodDisplayName'] == 'ExternalMobileTransfer' and 'Mobile QR' in SH['internalNotes']:
        return 'Mobile'
    elif SH['fulfillmentMethodDisplayName'] == 'Barcode' and 'E-Delivery' in SH['internalNotes']:
        return 'TicketFast'
    elif SH['fulfillmentMethodDisplayName'] == 'UPS' and 'E-Delivery' in SH['internalNotes']:
        return 'TicketFast'
    elif SH['fulfillmentMethodDisplayName'] == 'Barcode' and 'Food' in SH['internalNotes']:
        return 'TicketFast'
    elif SH['fulfillmentMethodDisplayName'] == 'PDF' and 'E-Delivery' in SH['internalNotes']:
        return 'TicketFast'
    elif SH['fulfillmentMethodDisplayName'] == 'PDF' and 'General' in SH['internalNotes']:
        return 'TicketFast'
    elif SH['fulfillmentMethodDisplayName'] == 'PDF' and 'Session' in SH['internalNotes']:
        return 'TicketFast'
    elif SH['fulfillmentMethodDisplayName'] == 'PDF' and '.' in SH['internalNotes']:
        return 'TicketFast'
    else:
        return SH['deliveryOption']
SH['Exchange Ticket Type'] = SH.apply(f, axis=1)
SH['Exchange Ticket Type'] = SH['Exchange Ticket Type'].replace(['BARCODE', 'EXTERNAL_TRANSFER', 'FLASHSEAT',
          'MOBILE_TICKET', 'PDF', 'UPS', 'LMS'],['Barcode', 'Mobile', 'Flash', 'Mobile', 'TicketFast', 'Mail', 'Mail'])
SHcols = ['fulfillmentMethodDisplayName', 'internalNotes', 'listingId', 'eventName_y', 'eventDate_y', 'quantity_y',
          'inHandDate_y', 'description', 'internalNote']
SH.drop(SHcols, axis=1, inplace=True)
SH = SH[['TB Ticket Type', 'Exchange Ticket Type', 'externalListingId', 'eventName_x', 'eventDate_x', 'venueName',
         'quantity_x', 'inHandDate_x', 'deliveryOption']]
SH = SH.rename(index=str, columns={"eventName_x":"eventName", "eventDate_x":"eventDate", "quantity_x":"quantity",
                                   "inHandDate_x":"inHandDate"})

#Identify Vivid Seats Ticket Types
VS = VS.rename(index=str, columns={"ticketId":"externalListingId"})
VS['externalListingId'] = VS['externalListingId'].astype(int)
VS = VS.merge(TBType, how='left', on = 'externalListingId')
def f(VS):
    if VS['stockType'] == 'ELECTRONIC' and 'QR' in VS['notes']:
        return 'Mobile'
    elif VS['stockType'] == 'ELECTRONIC' and VS['electronicTransfer'] == 'TRUE':
        return 'TicketFast'
    elif VS['stockType'] == 'FLASH':
        return 'Flash'
    elif VS['stockType'] == 'HARD':
        return 'Mail'
    elif VS['hasBarcodes'] == 'TRUE':
        return 'Barcode'
    elif VS['stockType'] == 'MOBILE_SCREENCAP':
        return 'Mobile'
    elif VS['stockType'] == 'TMET' and 'XFER' in VS['notes']:
        return 'Mobile'
    elif VS['stockType'] == 'TMET' and 'QR' in VS['notes']:
        return 'Mobile'
    else:
        return VS['stockType']
VS['Exchange Ticket Type'] = VS.apply(f, axis=1)
VS['Exchange Ticket Type'] = VS['Exchange Ticket Type'].replace(['ELECTRONIC'],['TicketFast'])
VScols = ['eventName_y', 'eventDate_y', 'quantity_y', 'description', 'inHandDate_y', 'internalNote', 'hasBarcodes',
          'electronicTransfer', 'notes']
VS.drop(VScols, axis=1, inplace=True)
VS = VS[['TB Ticket Type', 'Exchange Ticket Type', 'externalListingId', 'eventName_x', 'eventDate_x', 'venue',
         'quantity_x', 'inHandDate_x', 'stockType']]
VS = VS.rename(index=str, columns={"eventName_x":"eventName", "eventDate_x":"eventDate", "quantity_x":"quantity",
                                   "inHandDate_x":"inHandDate", "venue":"venueName"})

#Identify Ticket Network Direct Ticket Types
TN = TN.rename(index=str, columns={"ticketGroupId":"externalListingId"})
TN = TN.merge(TBType, how='left', on = 'externalListingId')
TN['Exchange Ticket Type'] = TN.ticketType
TN['Exchange Ticket Type'] = TN['Exchange Ticket Type'].replace(['Mobile', 'Standard', 'e-Ticket',
                                      'Flash Seats'],['Mobile', 'Mail', 'TicketFast', 'Flash'])
TNcols = ['eventName_y', 'eventDate_y', 'quantity_y', 'inHandDate', 'description', 'internalNote']
TN.drop(TNcols, axis=1, inplace=True)
TN = TN[['TB Ticket Type', 'Exchange Ticket Type', 'externalListingId', 'eventName_x', 'eventDate_x', 'venueName',
         'quantity_x', 'onHandDate', 'typeNotes']]
TN = TN.rename(index=str, columns={"eventName_x":"eventName", "eventDate_x":"eventDate", "quantity_x":"quantity"})

#Identify Ticketmaster ticket types
TM = TM.rename(index=str, columns={"supplierTicketId":"externalListingId"})
TM['externalListingId'] = TM['externalListingId'].astype(int)
TM = TM.merge(TBType, how='left', on = 'externalListingId')
TM['description_x'] = TM['description_x'].astype(str)
def f(TM):
    if 'Flash' in TM['description_x']:
        return 'Flash'
    elif 'XFER Mobile' in TM['description_x']:
        return 'Mobile'
    elif 'E-Delivery' in TM['description_x']:
        return 'TicketFast'
    elif 'Delivery' in TM['description_x']:
        return 'Mail'
    elif 'UPS' in TM['description_x']:
        return 'Mail'
    elif 'MobileQR' in TM['description_x']:
        return 'Barcode'
    else:
        return TM['description_x']
TM['Exchange Ticket Type'] = TM.apply(f, axis=1)
TM = TM[['TB Ticket Type', 'Exchange Ticket Type', 'eventName_x', 'eventDate_x', 'eventTime', 'venueName', 'productionId',
         'inHandDate_x', 'quantity', 'description_x', 'externalListingId']]
TM = TM.rename(index=str, columns={"eventName_x":"eventName", "eventDate_x":"eventDate", "description_x":"description",
                                   "inHandDate_x":"inHandDate"})

#Remove observations that are not discrepancies for each exchange
def f(SH):
   return 'yes' if SH['TB Ticket Type'] == SH['Exchange Ticket Type'] else 'no'
SH['Check'] = SH.apply(f, axis=1)
SH = SH[SH['Check'] == 'no']
SH.drop(['Check'], axis=1, inplace=True)

def f(VS):
   return 'yes' if VS['TB Ticket Type'] == VS['Exchange Ticket Type'] else 'no'
VS['Check'] = VS.apply(f, axis=1)
VS = VS[VS['Check'] == 'no']
VS.drop(['Check'], axis=1, inplace=True)

def f(TN):
   return 'yes' if TN['TB Ticket Type'] == TN['Exchange Ticket Type'] else 'no'
TN['Check'] = TN.apply(f, axis=1)
TN = TN[TN['Check'] == 'no']
TN.drop(['Check'], axis=1, inplace=True)

def f(TM):
   return 'yes' if TM['TB Ticket Type'] == TM['Exchange Ticket Type'] else 'no'
TM['Check'] = TM.apply(f, axis=1)
TM = TM[TM['Check'] == 'no']
TM.drop(['Check'], axis=1, inplace=True)

#Export dataframes to sheets in an excel workbook
writer = pd.ExcelWriter('F:/TB-Applications/Data/Deployed/TicketTypesReport/TicketTypes/ListingTicketTypes.xlsx', engine = 'xlsxwriter')
SH.to_excel(writer, sheet_name = 'StubHub', index = False)
TN.to_excel(writer, sheet_name = 'Ticket Network Direct', index = False)
VS.to_excel(writer, sheet_name = 'VividSeats', index = False)
TM.to_excel(writer, sheet_name = 'Ticketmaster', index = False)
workbook = writer.book
worksheet1 = writer.sheets['StubHub']
worksheet2 = writer.sheets['Ticket Network Direct']
worksheet3 = writer.sheets['VividSeats']
worksheet4 = writer.sheets['Ticketmaster']
worksheet1.set_column('A:I', 20)
worksheet2.set_column('A:I', 20)
worksheet3.set_column('A:I', 20)
worksheet4.set_column('A:I', 20)
writer.save()
print("Report Complete -- Close Window")

# Connect to the Outlook server
admin_email = 'noreply@ticketboat.com'
admin_password = 'RetroG@mes'
smtpObj = smtplib.SMTP('smtp-mail.outlook.com', 587)
smtpObj.ehlo()
smtpObj.starttls()
smtpObj.login(admin_email, admin_password)

# Create message container - the correct MIME type is multipart/alternative.
body = 'Please find the ticket type reports attached for current orders and listings.'
msg = MIMEMultipart('alternative')
msg['Subject'] = "Ticket Types Report"
msg['From'] = admin_email
master_recipients = ['apierson@ticketboat.com', 'ckesner@ticketboat.com', 'lindsay@ticketboat.com',
                     'lgregerson@ticketboat.com']
msg['To'] = ", ".join(master_recipients)
attachment = MIMEApplication(open('F:/TB-Applications/Data/Deployed/TicketTypesReport/TicketTypes/ListingTicketTypes.xlsx', 'rb').read(), _subtype='csv')
attachment.add_header('Content-Disposition', 'attachment', filename='ListingTicketTypes.xlsx')
attachment2 = MIMEApplication(open('F:/TB-Applications/Data/Deployed/TicketTypesReport/TicketTypes/OrderTicketTypes.xlsx', 'rb').read(), _subtype='doc')
attachment2.add_header('Content-Disposition', 'attachment', filename='OrderTicketTypes.xlsx')
attachment3 = MIMEApplication(open('F:/TB-Applications/Data/Deployed/TicketTypesReport/TicketTypes/Ticket Type Report Memo.docx', 'rb').read(), _subtype='doc')
attachment3.add_header('Content-Disposition', 'attachment', filename='Ticket Type Report Memo.docx')
email_body = MIMEText(body, 'plain')
msg.attach(attachment)
msg.attach(attachment2)
msg.attach(attachment3)
msg.attach(email_body)
smtpObj.sendmail(admin_email, master_recipients, msg.as_string())

sys.exit()