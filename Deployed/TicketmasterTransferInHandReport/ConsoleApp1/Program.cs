﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using RestSharp;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Xml;

namespace Ticketmaster_API_Listing
{
    class Program
    {
        static void Main(string[] args)
        {
            var client = new RestClient("https://api.eventinventory.com/BrokerAPI/V1/Inventory/?securityToken=21775A047A9F4227B71711E6E7FB658B&brokerId=1395&inventoryStatusTypeIDs=1,2&posInventoryStatus=1");

            client.Timeout = 500000;

            var request = new RestRequest(Method.GET);
            request.AddHeader("Postman-Token", "3ff849c6-c0ba-46d8-9573-e1c66fc34178");
            request.AddHeader("cache-control", "no-cache");
            IRestResponse response = client.Execute(request);


            using (System.IO.StreamWriter file = new System.IO.StreamWriter(@"F:\TB-Applications\Data\Deployed\TicketmasterTransferInHandReport\outputfiles\rrawresponse.txt"))

            {

                file.WriteLine(response.Content.ToString());

            }


            Console.WriteLine("done getting new file.");


            string xmlString = File.ReadAllText(@"F:\TB-Applications\Data\Deployed\TicketmasterTransferInHandReport\outputfiles\rrawresponse.txt");

            string origxmlstring = xmlString;

            xmlString = RemoveInvalidXmlChars(xmlString);





            StreamWriter writegoodfile = new StreamWriter(@"F:\TB-Applications\Data\Deployed\TicketmasterTransferInHandReport\outputfiles\nocontrolcresponse.txt");

            writegoodfile.WriteLine(xmlString);

            writegoodfile.Flush();

            writegoodfile.Close();

            XmlDocument xmlDoc = new XmlDocument();

            xmlDoc.LoadXml(xmlString);





            var numInventories = 0;

            XmlDocument doc = new XmlDocument();

            doc.LoadXml(xmlString);





            XmlNodeList list = doc.SelectNodes("//*");





            /* write out csv file for spreadsheet */

            StreamWriter writecsv = new StreamWriter(@"F:\TB-Applications\Data\Deployed\TicketmasterTransferInHandReport\outputfiles\inventories.csv");





            Dictionary<string, string> dictionaryelement = new Dictionary<string, string>();

            Dictionary<int, Dictionary<string, string>> andrewdictionary = new Dictionary<int, Dictionary<string, string>>();

            //List<ObjInventory> objInvList = new List<ObjInventory>();





            var nodeCount = 0;

            foreach (XmlNode rmxn in list)
            {

                if (rmxn.Name.Equals("a:Inventory"))
                {

                    numInventories++;

                    List<string> stringList = new List<string>();





                    string jsonString = JsonConvert.SerializeXmlNode(rmxn);

                    JObject objInventory = JObject.Parse(jsonString);

                    string aInventory = objInventory["a:Inventory"].ToString();

                    objInventory = JObject.Parse(aInventory);





                    Console.WriteLine("----------------------------------------------------------------------------");





                    Console.WriteLine("objInventory count : " + numInventories);





                    stringList.Add(objInventory["a:EventName"].ToString());

                    stringList.Add(objInventory["a:EventDate"].ToString());

                    stringList.Add(objInventory["a:EventTime"].ToString());

                    stringList.Add(objInventory["a:InHandDate"].ToString());

                    stringList.Add(objInventory["a:ProductionID"].ToString());

                    stringList.Add(objInventory["a:SupplierTicketID"].ToString());

                    stringList.Add(objInventory["a:Cost"].ToString());

                    stringList.Add(objInventory["a:Description"].ToString());

                    stringList.Add(objInventory["a:VenueName"].ToString());

                    stringList.Add(objInventory["a:InventoryStatus"].ToString());





                    dictionaryelement["a:EventName"] = objInventory["a:EventName"].ToString();

                    dictionaryelement["a:EventDate"] = objInventory["a:EventDate"].ToString();

                    dictionaryelement["a:EventTime"] = objInventory["a:EventTime"].ToString();

                    dictionaryelement["a:InHandDate"] = objInventory["a:InHandDate"].ToString();

                    dictionaryelement["a:ProductionID"] = objInventory["a:ProductionID"].ToString();

                    dictionaryelement["a:SupplierTicketID"] = objInventory["a:SupplierTicketID"].ToString();

                    dictionaryelement["a:Cost"] = objInventory["a:Cost"].ToString();

                    dictionaryelement["a:Description"] = objInventory["a:Description"].ToString();

                    dictionaryelement["a:VenueName"] = objInventory["a:VenueName"].ToString();

                    dictionaryelement["a:InventoryStatus"] = objInventory["a:InventoryStatus"].ToString();





                    andrewdictionary[nodeCount] = dictionaryelement;





                    //var objinventory2 = new ObjInventory(objInventory["a:EventName"].ToString()

                    //, objInventory["a:EventDate"].ToString()

                    //, objInventory["a:EventTime"].ToString()

                    //, objInventory["a:InHandDate"].ToString()

                    //, objInventory["a:ProductionID"].ToString()

                    //, objInventory["a:SupplierTicketID"].ToString()

                    //, objInventory["a:Cost"].ToString()

                    //, objInventory["a:Cost"].ToString()

                    //, objInventory["a:VenueName"].ToString());





                    //objInvList.Add(objinventory2);





                    writecsv.WriteLine(string.Join<string>(",", stringList));

                    writecsv.Flush();





                }

                nodeCount++;

            }





            writecsv.Close();

            Console.ReadLine();


        }
        public static string RemoveInvalidXmlChars(string content)

        {

            return new string(content.Where(ch => System.Xml.XmlConvert.IsXmlChar(ch)).ToArray());

        }
    }
}