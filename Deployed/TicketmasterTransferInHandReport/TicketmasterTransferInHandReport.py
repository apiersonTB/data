#Read in libraries
import os
import time
import pyodbc
import smtplib
import pandas as pd
from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart
from email.mime.application import MIMEApplication

#Database Pull for Ticket Boat TicketGroups
def data_pull(server='192.168.10.203', database='eiboxoffice', username='appuser', password='RetroG@mes', query=''):
    cnxn = pyodbc.connect('DRIVER={ODBC Driver 13 for SQL Server};SERVER='+server+';DATABASE='+database+';UID='+username
                          + ';PWD=' + password)
    return pd.read_sql_query(query, cnxn)
TB_TicketType_query = """SELECT E.[EventName] AS eventName
	  ,TG.[TicketGroupID] AS externalListingId
      ,TG.[EventDate] AS eventDate
      ,TG.[Quantity] AS quantity
      ,TG.[InHandDate] AS inHandDate
      ,TG.[Description] AS description
      ,TG.[InternalNote] AS internalNote
      ,TG.[IsInHand] AS isInHand
  FROM [eiboxoffice].[dbo].[TicketGroup] TG
  LEFT JOIN [eiboxoffice].[dbo].[Event] E ON E.EventID = TG.[PrimaryEventID]
  LEFT JOIN [eiboxoffice].[dbo].[Venue] V ON V.VenueID = TG.[VenueID]
  WHERE TG.[EventDate] >= GETDATE()"""
TBType = data_pull(server='192.168.10.203', query=TB_TicketType_query)

#API Call Using C# Program for Ticketmaster Listings
os.startfile("F:/TB-Applications/Data/Deployed/TicketmasterTransferInHandReport/ConsoleApp1/bin/Debug/ConsoleApp1.exe")
time.sleep(400)
TM = pd.read_csv('F:/TB-Applications/Data/Deployed/TicketmasterTransferInHandReport/outputfiles/inventories.csv',
                 names=["eventName","eventDate", "eventTime", "inHandDate", "productionId", "supplierTicketId", "cost",
                        "description", "venueName", "InventoryStatus"])

#Join eiBox Office listings to Ticketmaster Listings
TM = TM.rename(index=str, columns={"supplierTicketId":"externalListingId"})
TM['externalListingId'] = TM['externalListingId'].astype(int)
TM = TM.merge(TBType, how='left', on = 'externalListingId')
TM = TM[['eventName_x', 'eventDate_x', 'eventTime', 'venueName', 'inHandDate_x', 'quantity', 'internalNote',
         'description_x', 'productionId', 'externalListingId', 'isInHand']]
TM = TM.rename(index=str, columns={"eventName_x":"eventName", "eventDate_x":"eventDate", "description_x":"description",
                                   "inHandDate_x":"inHandDate", "isInHand":"eiBO isInHand"})

#Determine InHand Status
TM['inHandDate'] = pd.to_datetime(TM['inHandDate']).dt.normalize()
today = pd.datetime.today().date().strftime("%m/%d/%Y")
TM['Ticketmaster isInHand'] = TM['inHandDate'] <= today

#Filter data to contain only Ticketmaster isInHand = TRUE and isInHand
TM = TM[(TM['Ticketmaster isInHand'] == 1)]
TM = TM[(TM['eiBO isInHand'] != 1)]

#Build dataframe for workbook
TM = TM[['eventName', 'eventDate', 'eventTime', 'venueName', 'inHandDate', 'quantity', 'internalNote',
         'description', 'productionId', 'externalListingId', 'Ticketmaster isInHand', 'eiBO isInHand']]

TM.to_csv('F:/TB-Applications/Data/Deployed/TicketmasterTransferInHandReport/TicketmasterTransferInHandReport.csv', index=False)

# Connect to the Outlook server
admin_email = 'noreply@ticketboat.com'
admin_password = 'RetroG@mes'
smtpObj = smtplib.SMTP('smtp-mail.outlook.com', 587)
smtpObj.ehlo()
smtpObj.starttls()
smtpObj.login(admin_email, admin_password)

# Create message container - the correct MIME type is multipart/alternative.
body = 'Please find the Ticketmaster Transfer (InHand) Report attached.'
msg = MIMEMultipart('alternative')
msg['Subject'] = "Ticketmaster Transfer (InHand) Report"
msg['From'] = admin_email
master_recipients = ['apierson@ticketboat.com', 'zhutson@ticketboat.com', 'lgregerson@ticketboat.com']
msg['To'] = ", ".join(master_recipients)
attachment = MIMEApplication(open('F:/TB-Applications/Data/Deployed/TicketmasterTransferInHandReport/TicketmasterTransferInHandReport.csv', 'rb').read(), _subtype='csv')
attachment.add_header('Content-Disposition', 'attachment', filename='TicketmasterTransferInHandReport.csv')
email_body = MIMEText(body, 'plain')
msg.attach(attachment)
msg.attach(email_body)
smtpObj.sendmail(admin_email, master_recipients, msg.as_string())