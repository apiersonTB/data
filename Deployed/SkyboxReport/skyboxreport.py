import json
import smtplib
import datetime
import pandas as pd
from requests import get
from datetime import timedelta
from sqlalchemy import create_engine
from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart
from email.mime.application import MIMEApplication

#Define date filters
yesterday = datetime.date.today()- timedelta(1)
yesterday = yesterday.strftime('%Y-%m-%d')

#Call the SkyBox API
Data = []
InvoiceID = []
InternalID = []
InvoiceDate = []
CreatedBy = []
ExternalReference = []
Customer = []
StockType = []
ElectronicTransfer = []
InHandDate = []
PurchaseID = []
Vendor = []
Performer = []
Category = []
EventType = []
EventID = []
Event = []
EventDate = []
Venue = []
State = []
Section = []
Row = []
LowSeats = []
HighSeats =[]
QTY = []
TicketCost = []
UnitCost = []
TicketSales = []
ExpectedValue = []
PNL = []
PPNL = []
PublicNotes = []
InternalNotes = []
PaymentStatus = []
PaymentReference = []
FulfillmentStatus = []
ZoneSeating = []
Received = []
InventoryTags = []
InvoiceTags = []
pages = [str(i) for i in range(1,10)]
for page in pages:
    url = 'https://skybox.vividseats.com/services/inventory/sold?limit=10&sortDir=ASC&pageNumber='+page+'&sortedBy=EVENT_NAME&invoiceDateFrom='+yesterday+'T00:00:00.000Z&invoiceDateTo='+yesterday+'T23:59:59.000Z'
    headers = {"X-Api-Token":"9ecea294-c557-46ea-b064-c4effdd6c774",
               "X-Application-Token":"03b20561-14b4-41f0-a74a-54e8be53b313", "X-Account":"978"}
    response = get(url, headers=headers)
    response = response.text
    d2 = json.loads(response)
    Data.extend(d2['rows'])
    for i in d2['rows']:
        try:
            EventID.append(i['eventId'])
        except:
            EventID.append(i['eventId'])
        try:
            InvoiceDate.append(i['invoiceDate'])
        except:
            InvoiceDate.append(i['invoiceDate'])
        try:
            CreatedBy.append(i['createdBy'])
        except:
            CreatedBy.append(i['createdBy'])
        try:
            ExternalReference.append(i['invoiceExternalRef'])
        except:
            ExternalReference.append(i['invoiceExternalRef'])
        try:
            Customer.append(i['customerDisplayName'])
        except:
            Customer.append(i['customerDisplayName'])
        try:
            StockType.append(i['stockType'])
        except:
            StockType.append(i['stockType'])
        try:
            ElectronicTransfer.append(i['electronicTransfer'])
        except:
            ElectronicTransfer.append(i['electronicTransfer'])
        try:
            InHandDate.append(i['inHandDate'])
        except:
            InHandDate.append(i['inHandDate'])
        try:
            PurchaseID.append(i['purchaseIds'][0])
        except:
            PurchaseID.append(i['purchaseIds'][0])
        try:
            Vendor.append(i['vendor'])
        except:
            Vendor.append(i['vendor'])
        try:
            Performer.append(i['event']['performer']['name'])
        except:
            Performer.append(i['event']['performer']['name'])
        try:
            Category.append(i['event']['performer']['category']['name'])
        except:
            Category.append(i['event']['performer']['category']['name'])
        try:
            Event.append(i['event']['name'])
        except:
            Event.append(i['event']['name'])
        try:
            EventDate.append(i['event']['date'])
        except:
            EventDate.append(i['event']['date'])
        try:
            Venue.append(i['event']['venue']['name'])
        except:
            Venue.append(i['event']['venue']['name'])
        try:
            State.append(i['event']['venue']['state'])
        except:
            State.append(i['event']['venue']['state'])
        try:
            Section.append(i['section'])
        except:
            Section.append(i['section'])
        try:
            Row.append(i['row'])
        except:
            Row.append(i['row'])
        try:
            LowSeats.append(i['lowSeat'])
        except:
            LowSeats.append(i['lowSeat'])
        try:
            HighSeats.append(i['highSeat'])
        except:
            HighSeats.append(i['highSeat'])
        try:
            InvoiceID.append(i['invoiceId'])
        except:
            InvoiceID.append(i['invoiceId'])
        try:
            QTY.append(i['quantity'])
        except:
            QTY.append(i['quantity'])
        try:
            TicketCost.append(i['cost'])
        except:
            TicketCost.append(i['cost'])
        try:
            UnitCost.append(i['unitCostAverage'])
        except:
            UnitCost.append(i['unitCostAverage'])
        try:
            TicketSales.append(i['total'])
        except:
            TicketSales.append(i['total'])
        try:
            PNL.append(i['profit'])
        except:
            PNL.append(i['profit'])
        try:
            PPNL.append(i['profitMargin'])
        except:
            PPNL.append(i['profitMargin'])
        try:
            PublicNotes.append(i['publicNotes'])
        except:
            PublicNotes.append(i['publicNotes'])
        try:
            InternalNotes.append(i['notes'])
        except:
            InternalNotes.append(i['notes'])
        try:
            PaymentStatus.append(i['paymentStatus'])
        except:
            PaymentStatus.append(i['paymentStatus'])
        try:
            FulfillmentStatus.append(i['fulfillmentStatus'])
        except:
            FulfillmentStatus.append(i['fulfillmentStatus'])
        try:
            ZoneSeating.append(i['zoneSeating'])
        except:
            ZoneSeating.append(i['zoneSeating'])
        try:
            Received.append(i['received'])
        except:
            Received.append(i['received'])
        try:
            InventoryTags.append(i['tags'])
        except:
            InventoryTags.append(i['tags'])
        try:
            InvoiceTags.append(i['tags'])
        except:
            InvoiceTags.append(i['tags'])

SB = pd.DataFrame()
SB['Invoice ID'] = InvoiceID
SB['Invoice Date'] = InvoiceDate
SB['Created By'] = CreatedBy
SB['External Reference'] = ExternalReference
SB['Customer'] = Customer
SB['Stock Type'] = StockType
SB['Electronic Transfer'] = ElectronicTransfer
SB['In-Hand Date'] = InHandDate
SB['Purchase ID'] = PurchaseID
SB['Vendor'] = Vendor
SB['Performer'] = Performer
SB['Category'] = Category
SB['Event ID'] = EventID
SB['Event'] = Event
SB['Event Date'] = EventDate
SB['Venue'] = Venue
SB['State'] = State
SB['Section'] = Section
SB['Row'] = Row
SB['Low Seats'] = LowSeats
SB['High Seats'] = HighSeats
SB['Quantity'] = QTY
SB['Ticket Cost'] = TicketCost
SB['Ticket Cost'] = SB['Ticket Cost'].apply(lambda x: '${:,.2f}'.format(x))
SB['Unit Cost'] = UnitCost
SB['Unit Cost'] = SB['Unit Cost'].apply(lambda x: '${:,.2f}'.format(x))
SB['Ticket Sales'] = TicketSales
SB['Ticket Sales'] = SB['Ticket Sales'].apply(lambda x: '${:,.2f}'.format(x))
SB['P&L'] = PNL
SB['P&L'] = SB['P&L'].apply(lambda x: '${:,.2f}'.format(x))
SB['% P&L'] = PPNL
SB['% P&L'] = SB['% P&L'].apply(lambda x: '%{:,.2f}'.format(x))
SB['Public Notes'] = PublicNotes
SB['Internal Notes'] = InternalNotes
SB['Payment Status'] = PaymentStatus
SB['Fulfillment Status'] = FulfillmentStatus
SB['Zone Seating'] = ZoneSeating
SB['Received'] = Received
SB['Inventory Tags'] = InventoryTags
SB['Invoice Tags'] = InvoiceTags
SB.to_csv('F:/TB-Applications/Data/Deployed/SkyboxReport/skybox.csv', index=False)

#Write to sql table
engine = create_engine("mssql+pyodbc://appuser:RetroG@mes@192.168.10.209/STG?DRIVER={ODBC Driver 13 for SQL Server}")
SB.to_sql(name='skyboxsoldreport', con=engine, schema='dbo', if_exists='replace', index=False)

# Connect to the Outlook server
admin_email = 'noreply@ticketboat.com'
admin_password = 'RetroG@mes'
smtpObj = smtplib.SMTP('smtp-mail.outlook.com', 587)
smtpObj.ehlo()
smtpObj.starttls()
smtpObj.login(admin_email, admin_password)

# Create message container - the correct MIME type is multipart/alternative.
body = 'Please find yesterdays skybox report attached.'
msg = MIMEMultipart('alternative')
msg['Subject'] = "Skybox Sold Report"
msg['From'] = admin_email
master_recipients = ['apierson@ticketboat.com', 'mike@ticketboat.com', 'bbrown@ticketboat.com',
                     'rsharemet@ticketboat.com', 'Mark@ticketboat.com', 'shane@ticketboat.com',
                     'choneyman@ticketboat.com', 'tderrickson@ticketboat.com', 'sroque@ticketboat.com',
                     'lgregerson@ticketboat.com']
msg['To'] = ", ".join(master_recipients)
attachment = MIMEApplication(open('F:/TB-Applications/Data/Deployed/SkyboxReport/skybox.csv', 'rb').read(), _subtype='csv')
attachment.add_header('Content-Disposition', 'attachment', filename='SkyboxSoldReport.csv')
email_body = MIMEText(body, 'plain')
msg.attach(attachment)
msg.attach(email_body)
smtpObj.sendmail(admin_email, master_recipients, msg.as_string())
