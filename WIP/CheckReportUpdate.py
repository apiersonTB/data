import os.path
import time
import pandas as pd
import pyodbc
import smtplib
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText

#Define the function for pulling data from SQL Server database
def data_pull(server='192.168.10.209', database='STG', username='appuser', password='RetroG@mes', query=''):
    cnxn = pyodbc.connect('DRIVER={ODBC Driver 13 for SQL Server};SERVER='+server+';DATABASE='+database+';UID='+username
                          + ';PWD=' + password)
    return pd.read_sql_query(query, cnxn)

#Define query to pull create date of data
APForecasts = """SELECT create_date FROM sys.objects WHERE Name='APForecasts'"""
APForecasts = data_pull(server='192.168.10.209', query=APForecasts)
APForecasts = APForecasts['create_date'][0]
SALTForecasts = """SELECT create_date FROM sys.objects WHERE Name='SaltForecasts'"""
SALTForecasts = data_pull(server='192.168.10.209', query=SALTForecasts)
SALTForecasts = SALTForecasts['create_date'][0]
EmployeeDirectory = """SELECT create_date FROM sys.objects WHERE Name='employeedirectory'"""
EmployeeDirectory = data_pull(server='192.168.10.209', query=EmployeeDirectory)
EmployeeDirectory = EmployeeDirectory['create_date'][0]

UnshippedReport = time.ctime(os.path.getmtime("F:/TB-Applications/Data/Deployed/UnshippedReport/unshippedreportoutputfiles/Unshipped Report/UnshippedReport.xlsx"))
TEVOReport = time.ctime(os.path.getmtime("F:/TB-Applications/Data/Deployed/TicketEvolutionReport/Quickbooks and Errors/TicketEvolutionPOs.xlsx"))
SkyboxReport = time.ctime(os.path.getmtime("F:/TB-Applications/Data/Deployed/SkyboxReport/skybox.csv"))
ListingTicketTypeReport = time.ctime(os.path.getmtime("F:/TB-Applications/Data/Deployed/TicketTypesReport/TicketTypes/ListingTicketTypes.xlsx"))
OrderTicketTypeReport = time.ctime(os.path.getmtime("F:/TB-Applications/Data/Deployed/TicketTypesReport/TicketTypes/OrderTicketTypes.xlsx"))
TicketmasterTransferReport = time.ctime(os.path.getmtime("F:/TB-Applications/Data/Deployed/TicketmasterTransferInHandReport/TicketmasterTransferInHandReport.csv"))
APDataRefresh = time.ctime(os.path.getmtime("C:/Users/administrator.TICKETBOAT0/Ticket Boat/Data Analytics - Documents/APDATA.csv"))
LeadListJIT = time.ctime(os.path.getmtime("F:/TB-Applications/Data/Deployed/LeadList/JustInTime.xlsm"))
LeadListL7D = time.ctime(os.path.getmtime("F:/TB-Applications/Data/Deployed/LeadList/Last7Days.xlsm"))
LeadListN30 = time.ctime(os.path.getmtime("F:/TB-Applications/Data/Deployed/LeadList/Next30.xlsm"))
LeadListTSE = time.ctime(os.path.getmtime("F:/TB-Applications/Data/Deployed/LeadList/TopSellingEvents.xlsm"))
LeadListTTGT = time.ctime(os.path.getmtime("F:/TB-Applications/Data/Deployed/LeadList/ToursToGoThrough.xlsm"))
LeadListV = time.ctime(os.path.getmtime("F:/TB-Applications/Data/Deployed/LeadList/Viagogo.xlsm"))

Columns = ['Report/Data', 'Scheduled Run Time', 'Last Time Modified']
ReportScheduleUpdate = [['Unshipped Report', 'Daily 6:30AM', UnshippedReport],
                        ['Ticket Evolution Report', 'Daily 8:50AM', TEVOReport],
                        ['Skybox Report', 'Daily 6:00AM', SkyboxReport],
                        ['Listing Ticket Type Report', 'Daily 7:00AM', ListingTicketTypeReport],
                        ['Order Ticket Type Report', 'Daily 7:00AM', OrderTicketTypeReport],
                        ['Auto Transfer Report/dbo.APForecasts', 'Daily 2:00AM', APForecasts],
                        ['Auto Transfer Report/dbo.SaltForecasts', 'Daily 2:00AM', SALTForecasts],
                        ['Employee Directory', 'Daily 3:00AM', EmployeeDirectory],
                        ['Ticketmaster Transfer (InHand) Report', 'Daily 6:00AM', TicketmasterTransferReport],
                        ['AP Data Refresh (Webscraper)', 'Daily 1:00AM', APDataRefresh],
                        ['Lead List Just In Time', 'Daily 8:00AM', LeadListJIT],
                        ['Lead List Last 7 Days', 'Daily 8:00AM', LeadListL7D],
                        ['Lead List Next 30', 'Daily 8:00AM', LeadListN30],
                        ['Lead List Top Selling Events', 'Daily 8:00AM', LeadListTSE],
                        ['Lead List Tours To Go Through', 'Daily 8:00AM', LeadListTTGT],
                        ['Lead List Viagogo', 'Daily 8:00AM', LeadListV]]

Reports = pd.DataFrame(ReportScheduleUpdate, columns=Columns)
Reports['Last Time Modified'] = pd.to_datetime(Reports['Last Time Modified']).apply(lambda x: x.strftime('%m/%d/%Y %H:%M:%S'))

#Save as HTML
Reports.to_html('F:/TB-Applications/Data/WIP/ReportUpdates/ReportUpdates.html', index=False)

# Connect to the Outlook server
admin_email = 'noreply@ticketboat.com'
admin_password = 'RetroG@mes'
smtpObj = smtplib.SMTP('smtp-mail.outlook.com', 587)
smtpObj.ehlo()
smtpObj.starttls()
smtpObj.login(admin_email, admin_password)

# Create the master email for all events
master_html_file = open('F:/TB-Applications/Data/WIP/ReportUpdates/ReportUpdates.html', 'r')
master_msg = MIMEMultipart('alternative')
master_msg['Subject'] = "Reports Schedule"
master_msg['From'] = admin_email
master_recipients = ['apierson@ticketboat.com', 'timmderrickson@gmail.com', 'lgregerson@ticketboat.com']
master_msg['To'] = ", ".join(master_recipients)
master_email_body = MIMEText(master_html_file.read(), 'html')
master_msg.attach(master_email_body)
smtpObj.sendmail(admin_email, master_recipients, master_msg.as_string())
smtpObj.quit()